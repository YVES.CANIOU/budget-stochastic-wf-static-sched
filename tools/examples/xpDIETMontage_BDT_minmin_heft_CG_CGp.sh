#!/bin/bash
seeds="0 2 3 4 5 6 7 8 9 10 642 775 35 471 807 26328 1441 5121 27830 15283 30071 5785 9096 26968 16110 2365 17341 11932 15859 11088"
nums=`seq 1 1 30`

for id in `seq 0 1 29`; do
    #id=`echo "scale=4;$num-16"|bc`
    arrNums=($nums)
    num=${arrNums[$id]}
    arrSeeds=($seeds)
    seed=${arrSeeds[$id]}
    echo "idx = $id ; shape = $num ; seed = $seed"

    # minmin
    echo "minmin, data/workflows/Montage/montage30_$num"
    for budget in  `seq 65 25 190` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex minmin $seed  data/workflows/Montage/montage30_$num data/platforms/AL60DIET.xml 0.25 0.0 900 $budget 0.2
	mkdir montage30_$num/MM/
	mkdir montage30_$num/MM/$budget
        cp static_sched montage30_$num/MM/$budget/static_sched$budget
	make resetGen
    done

    # BDT
    echo "BDT"
    for budget in  `seq 65 25 190` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex arabnejad $seed  data/workflows/Montage/montage30_$num data/platforms/AL60DIET.xml 0.25 0.0 900 $budget 0.2
	mkdir montage30_$num/BDT/
	mkdir montage30_$num/BDT/$budget
        cp static_sched montage30_$num/BDT/$budget/static_sched$budget
	make resetGen
    done

    # heft
    echo "heft"
    for budget in  `seq 65 25 190` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex heft $seed  data/workflows/Montage/montage30_$num data/platforms/AL60DIET.xml 0.25 0.0 900 $budget 0.2
	mkdir montage30_$num/HH/
	mkdir montage30_$num/HH/$budget
        cp static_sched montage30_$num/HH/$budget/static_sched$budget
	make resetGen
    done

    # CG
    echo "CG"
    for budget in  `seq 65 25 190` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex CGFair $seed  data/workflows/Montage/montage30_$num data/platforms/AL60DIET.xml 0.25 0.0 900 $budget 0.2
	mkdir montage30_$num/CG/
	mkdir montage30_$num/CG/$budget
        cp static_sched montage30_$num/CG/$budget/static_sched$budget
	make resetGen
    done

    # CGp
    echo "CGp"
    for budget in  `seq 65 25 190` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex CGFairSecRep $seed  data/workflows/Montage/montage30_$num data/platforms/AL60DIET.xml 0.25 0.0 900 $budget 0.2
	mkdir montage30_$num/CGp/
	mkdir montage30_$num/CGp/$budget
        cp static_sched montage30_$num/CGp/$budget/static_sched$budget
	make resetGen
    done
    
done
