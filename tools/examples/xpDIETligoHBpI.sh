#!/bin/bash
seeds="9022 15295 6842 15068 490 15593 27709 15494 16290 17932 22035 13051 14475 14588 24671 4313 9972 26668 16120 6563 22669 13679 3380 2594 31220 7311 4573 24358 20675 27256"
for num in `seq 1 1 30`; do
    id=`echo "scale=4;$num-1"|bc`
    arrSeeds=($seeds)
    seed=${arrSeeds[$id]}
    echo "idx = $id ; shape = $num ; seed = $seed"

    # heftBudg+Inv
    echo "heftBudg+Inv"
    for budget in  `seq 1100 100 1600` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex heftSecRepInv $seed  data/workflows/ligo/ligo30_$num data/platforms/AL60DIET.xml 0.25 0.0 900 $budget 0.2
        cp static_sched ligo30_$num/HBpI/$budget/static_sched$budget
	make resetGen
    done   
done
