#!/bin/bash
seeds="9022 15295 6842 15068 490 15593 27709 15494 16290 17932 22035 13051 15475 14588 24671 4313 9972 26668 13120 6563 22669 13679 3380 2594 31220 7311 4573 24358 20675 27256"
nums=`seq 1 1 30`

for id in `seq 0 1 29`; do
    #id=`echo "scale=4;$num-16"|bc`
    arrNums=($nums)
    num=${arrNums[$id]}
    arrSeeds=($seeds)
    seed=${arrSeeds[$id]}
    echo "idx = $id ; shape = $num ; seed = $seed"

    # heftBudg+Inv
    echo "heftBudg+Inv"
    for budget in  `seq 1100 100 1600` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex heftSecRepInv $seed  data/workflows/ligo/ligo30_$num data/platforms/AL60DIET_infinite_bw.xml 0.25 0.0 900 $budget 0.2
	make resetGen
    done

    # heftBudg+
    echo "heftBudg+"
    for budget in  `seq 1100 100 1600` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex heftSecRep $seed  data/workflows/ligo/ligo30_$num data/platforms/AL60DIET_infinite_bw.xml 0.25 0.0 900 $budget 0.2
	make resetGen
    done

    # heftBudg
    echo "heftBudg"
    for budget in  `seq 1100 100 1600` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex heftBudg $seed  data/workflows/ligo/ligo30_$num data/platforms/AL60DIET_infinite_bw.xml 0.25 0.0 900 $budget 0.2
	make resetGen
    done
    
    # heftBudgMult
    echo "heftBudgMult"
    for budget in  `seq 1100 100 1600` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex heftBudgMult $seed  data/workflows/ligo/ligo30_$num data/platforms/AL60DIET_infinite_bw.xml 0.25 0.0 900 $budget 0.2
	make resetGen
    done

    # minminBudg
    echo "minminBudg"
    for budget in  `seq 1100 100 1600` ; do
	echo "      budget = $budget , seed = $seed , num = $num"
        bin/hex minminBudg $seed  data/workflows/ligo/ligo30_$num data/platforms/AL60DIET_infinite_bw.xml 0.25 0.0 900 $budget 0.2
	make resetGen
    done
    
done
