#!/bin/bash
# https://fr.wikipedia.org/wiki/Algorithme_de_Strassen

# WARNING: graphviz requires giving 'Mother -> Sister' as relation. Yet we
# do not know job_ids of sisters. We use reverse, and a the end, put things
# in order.

if [ $(grep TODO $0 | grep -v grep | wc -l) -ne 0 ]; then
    echo WARNING: still some todo in the code
fi

function usage {
    echo "Usage: $0 { -l lvl | -h N | -n N:Nmax | -m lvl:Nmax } [-g]"
    echo lvl is recursion level
    echo N is size of original N*N matrices, considered as amount of data
    echo Nmax is the max size of Nmax*Nmax matrices, e.g., 10-500
    echo -g for graphviz output 
}    


# Strassen: records all operations, products AND sums, as different tasks
function strassenAll {

    if [ $1 -eq 0 ]; then
	echo Strassen for at least 1 recursion
	return
    fi
    
    local niv=$1 # Level of recursion we are in, among [1:LVL]
    local A=$2
    local B=$3
    local n=$4 # nbrow of input square matrices
    local __parentCxy=$5 # name of parent variable to store Cx,y job_ids
    # $6 and possible $7 are the job_id of parent (linked by data)
    
    local sizemat=$(echo "$n * $n * 8" | bc) # 8 bytes for a double
    local sizeqmat=$(( sizemat / 4 ))
    local Cxy_parent=""
    
    echo "Recursion ${niv} for $A * $B"
    ##### M1
    M11="${niv}_[M1(${A}11+${A}22)]"
    #M11="${niv}_[M1(A11+A22)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    M13_parent="$job_id "
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${M11}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${A}11_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${A}22_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${M11}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    i=$6
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
    if [ $gz -eq 1 ]; then
	echo "$job_id => $i # ${M11}" >> "${GZfilename}"
    fi
    echo "</child>" >> "${WFfilename}Part3"
	
    M12="${niv}_[M1(${B}11+${B}22)]"
    #M12="${niv}_[M1(B11+B22)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    M13_parent+="$job_id "
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${M12}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${B}11_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${B}22_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${M12}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    i=$7
    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
    if [ $gz -eq 1 ]; then
	echo "$job_id => $i # ${M12}" >> "${GZfilename}"
    fi
    echo "</child>" >> "${WFfilename}Part3"
    
    M13="${niv}_[M1($A*$B)]"
    #M13="${niv}_[M1(A*B)]"
    M13_recursionDP="" # will contain Cx,y recursion job_ids
    if [ ${niv} -ne $LVL ]; then
	strassenAll $(( niv + 1)) "${M11}" "${M12}" $((n/2)) M13_recursionDP ${M13_parent}
	Cxy_jobids+="$M13_recursionDP "
    else
	job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
	JOBCOUNT=$(( JOBCOUNT + 1 ))
	Cxy_jobids+="$job_id "
	# Part description of job
	echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${M13}\" version=\"1.0\" level=\"0\" runtime=\"$durationDGEMM\">" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M11}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M12}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M13}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "</job>" >> "${WFfilename}Part2"
	# Part job dependancies
	echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
	for i in $M13_parent; do
	    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	    if [ $gz -eq 1 ]; then
		echo "$job_id => $i # ${M13}" >> "${GZfilename}"
	    fi
	done
	echo "</child>" >> "${WFfilename}Part3"
    fi
    Cxy_parent+="$Cxy_jobids "
    Cxy_jobids=""
    
    # ##### M2
    M21="${niv}_[M2(${A}21+${A}22)]"
    #M21="${niv}_[M2(A21+A22)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    M22_parent="$7 $job_id "
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${M21}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${A}21_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${A}22_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${M21}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    i=$6
    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
    if [ $gz -eq 1 ]; then
	echo "$job_id => $i # ${M21}" >> "${GZfilename}"
    fi
    echo "</child>" >> "${WFfilename}Part3"

    M22="${niv}_[M2($A*$B)]"
    #M22="${niv}_[M2(A*B)]"
    M22_recursionDP="" # will contain Cx,y recursion job_ids
    if [ ${niv} -ne $LVL ]; then
	strassenAll $(( niv + 1)) "${M21}" "M2(B)_${niv}" $((n/2)) M22_recursionDP $M22_parent
	Cxy_jobids+="$M22_recursionDP "
    else
	job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
	JOBCOUNT=$(( JOBCOUNT + 1 ))
	Cxy_jobids+="$job_id "
	# Part description of job
	echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${M22}\" version=\"1.0\" level=\"0\" runtime=\"$durationDGEMM\">" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M21}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${B}11_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M22}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "</job>" >> "${WFfilename}Part2"
	# Part job dependancies
	echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
	for i in $M22_parent; do
	    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	    if [ $gz -eq 1 ]; then
		echo "$job_id => $i # ${M22}" >> "${GZfilename}"
	    fi
	done
	echo "</child>" >> "${WFfilename}Part3"
    fi
    Cxy_parent+="$Cxy_jobids "
    Cxy_jobids=""

    # ##### M3
    M31="${niv}_[M3(${B}12-${B}22)]"
    #M31="${niv}_[M3(B12-B22)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    M32_parent="$6 $job_id "
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${M31}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${B}12_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${B}22_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${M31}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    i=$7
    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
    if [ $gz -eq 1 ]; then
	echo "$job_id => $i # ${M31}" >> "${GZfilename}"
    fi
    echo "</child>" >> "${WFfilename}Part3"
	    
    M32="${niv}_[M3($A*$B)]"
    #M32="${niv}_[M3(A*B)]"
    M32_recursionDP="" # will contain Cx,y recursion job_ids
    if [ ${niv} -ne $LVL ]; then
	strassenAll $(( niv + 1)) "M3(A)_${niv}" "${M31}" $((n/2)) M32_recursionDP ${M32_parent}
	Cxy_jobids+="$M32_recursionDP "
    else
	job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
	JOBCOUNT=$(( JOBCOUNT + 1 ))
	Cxy_jobids+="$job_id "
	# Part description of job
	echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${M32}\" version=\"1.0\" level=\"0\" runtime=\"$durationDGEMM\">" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${A}11_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M31}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M32}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "</job>" >> "${WFfilename}Part2"
	# Part job dependancies
	echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
	for i in $M32_parent; do
	    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	    if [ $gz -eq 1 ]; then
		echo "$job_id => $i # ${M32}" >> "${GZfilename}"
	    fi
	done
	echo "</child>" >> "${WFfilename}Part3"
    fi
    Cxy_parent+="$Cxy_jobids "
    Cxy_jobids=""

    # ##### M4
    M41="${niv}_[M4(${B}21-${B}11)]"
    #M41="${niv}_[M4(B21-B11)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    M42_parent="$6 $job_id "
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${M41}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${B}21_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${B}11_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${M41}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    i=$7
    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
    if [ $gz -eq 1 ]; then
	echo "$job_id => $i # ${M41}" >> "${GZfilename}"
    fi
    echo "</child>" >> "${WFfilename}Part3"
	    
    M42="${niv}_[M4($A*$B)]"
    #M42="${niv}_[M4(A*B)]"
    M42_recursionDP="" # will contain Cx,y recursion job_ids
    if [ ${niv} -ne $LVL ]; then
	strassenAll $(( niv + 1)) "M4(A)_${niv}" "${M41}" $((n/2)) M42_recursionDP ${M42_parent}
	Cxy_jobids+="$M42_recursionDP "
    else
	job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
	JOBCOUNT=$(( JOBCOUNT + 1 ))
	Cxy_jobids+="$job_id "
	# Part description of job
	echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${M42}\" version=\"1.0\" level=\"0\" runtime=\"$durationDGEMM\">" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${A}22_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M41}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M42}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "</job>" >> "${WFfilename}Part2"
	# Part job dependancies
	echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
	for i in $M42_parent; do
	    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	    if [ $gz -eq 1 ]; then
		echo "$job_id => $i # ${M42}" >> "${GZfilename}"
	    fi
	done
	echo "</child>" >> "${WFfilename}Part3"
    fi
    Cxy_parent+="$Cxy_jobids "
    Cxy_jobids=""

    # ##### M5
    M51="${niv}_[M5(${A}11+${A}12)]"
    #M51="${niv}_[M5(A11+A12)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    M52_parent="$7 $job_id "
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${M51}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${A}11_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${A}12_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${M51}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    i=$6
    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
    if [ $gz -eq 1 ]; then
	echo "$job_id => $i # ${M51}" >> "${GZfilename}"
    fi
    echo "</child>" >> "${WFfilename}Part3"
    
    M52="${niv}_[M5(${A}*${B})]"
    #M52="${niv}_[M5(A*B)]"
    M52_recursionDP="" # will contain Cx,y recursion job_ids
    if [ ${niv} -ne $LVL ]; then
	strassenAll $(( niv + 1)) "${M51}" "M5(B)_${niv}" $((n/2)) M52_recursionDP ${M52_parent}
	Cxy_jobids+="$M52_recursionDP "
    else
	job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
	JOBCOUNT=$(( JOBCOUNT + 1 ))
	Cxy_jobids+="$job_id "
	# Part description of job
	echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${M52}\" version=\"1.0\" level=\"0\" runtime=\"$durationDGEMM\">" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M51}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${B}22_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M52}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "</job>" >> "${WFfilename}Part2"
	# Part job dependancies
	echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
	for i in $M52_parent; do
	    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	    if [ $gz -eq 1 ]; then
		echo "$job_id => $i # ${M52}" >> "${GZfilename}"
	    fi
	done
	echo "</child>" >> "${WFfilename}Part3"
    fi
    Cxy_parent+="$Cxy_jobids "
    Cxy_jobids=""

    # ##### M6
    M61="${niv}_[M6(${A}21-${A}11)]"
    #M61="${niv}_[M6(A21-A11)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    M63_parent="$job_id "
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${M61}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${A}21_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${A}11_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${M61}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    i=$6
    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
    if [ $gz -eq 1 ]; then
	echo "$job_id => $i # ${M61}" >> "${GZfilename}"
    fi
    echo "</child>" >> "${WFfilename}Part3"

    M62="${niv}_[M6(${B}11+${B}12)]"
    #M62="${niv}_[M6(B11+B12)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    M63_parent+="$job_id "
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${M62}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${B}11_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${B}12_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${M62}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    i=$7
    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
    if [ $gz -eq 1 ]; then
	echo "$job_id => $i # ${M62}" >> "${GZfilename}"
    fi
    echo "</child>" >> "${WFfilename}Part3"
    
    M63="${niv}_[M6($A*$B)]"
    #M63="${niv}_[M6(A*B)]"
    M63_recursionDP="" # will contain Cx,y recursion job_ids
    if [ ${niv} -ne $LVL ]; then
	strassenAll $(( niv + 1)) "${M61}" "${M62}" $((n/2)) M63_recursionDP ${M63_parent}
	Cxy_jobids+="$M63_recursionDP "
    else
	job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
	JOBCOUNT=$(( JOBCOUNT + 1 ))
	Cxy_jobids+="$job_id "
	# Part description of job
	echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${M63}\" version=\"1.0\" level=\"0\" runtime=\"$durationDGEMM\">" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M61}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M62}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M63}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "</job>" >> "${WFfilename}Part2"
	# Part job dependancies
	echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
	for i in $M63_parent; do
	    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	    if [ $gz -eq 1 ]; then
		echo "$job_id => $i # ${M63}" >> "${GZfilename}"
	    fi
	done
	echo "</child>" >> "${WFfilename}Part3"
    fi
    Cxy_parent+="$Cxy_jobids "
    Cxy_jobids=""
    
    # ##### M7
    M71="${niv}_[M7(${A}12-${A}22)]"
    #M71="${niv}_[M7(A12-A22)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    M73_parent="$job_id "
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${M71}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${A}12_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${A}22_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${M71}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    i=$6
    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
    if [ $gz -eq 1 ]; then
	echo "$job_id => $i # ${M71}" >> "${GZfilename}"
    fi
    echo "</child>" >> "${WFfilename}Part3"
	
    M72="${niv}_[M7(${B}21+${B}22)]"
    #M72="${niv}_[M7(B21+B22)]"
    job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
    JOBCOUNT=$(( JOBCOUNT + 1 ))
    M73_parent+="$job_id "
    # Part description of job
    echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${M72}\" version=\"1.0\" level=\"0\" runtime=\"${tabsum[${niv}]}\">" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${B}21_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${B}22_${niv}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "  <uses file=\"T${M72}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
    echo "</job>" >> "${WFfilename}Part2"
    # Part job dependancies
    echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
    i=$7
    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
    if [ $gz -eq 1 ]; then
	echo "$job_id => $i # ${M72}" >> "${GZfilename}"
    fi
    echo "</child>" >> "${WFfilename}Part3"
    
    M73="${niv}_[M7($A*$B)]"
    #M73="${niv}_[M7(A*B)]"
    M73_recursionDP="" # will contain Cx,y recursion job_ids
    if [ ${niv} -ne $LVL ]; then
	strassenAll $(( niv + 1)) "${M71}" "${M72}" $((n/2)) M73_recursionDP ${M73_parent}
	Cxy_jobids+="$M73_recursionDP "
    else
	job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
	JOBCOUNT=$(( JOBCOUNT + 1 ))
	Cxy_jobids+="$job_id "
	# Part description of job
	echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"${M73}\" version=\"1.0\" level=\"0\" runtime=\"$durationDGEMM\">" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M71}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M72}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M73}\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "</job>" >> "${WFfilename}Part2"
	# Part job dependancies
	echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
	for i in $M73_parent; do
	    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	    if [ $gz -eq 1 ]; then
		echo "$job_id => $i # ${M73}" >> "${GZfilename}"
	    fi
	done
	echo "</child>" >> "${WFfilename}Part3"
    fi
    Cxy_parent+="$Cxy_jobids "
    Cxy_jobids=""
    
    ##### Cxy
    if [ ${niv} -eq 1 ]; then
	# Finish graph
	job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
	JOBCOUNT=$(( JOBCOUNT + 1 ))
	# Part description of job
	local duration12sums=$(echo "${tabsum[$((niv + 1))]} * 12" | bc | awk '{printf "%f", $0}')
	echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"FINISH\" version=\"1.0\" level=\"0\" runtime=\"${duration12sums}\">" >> "${WFfilename}Part2"
	# C11
	echo "  <uses file=\"T${M13}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M42}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M52}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M73}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	# C12
	echo "  <uses file=\"T${M32}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
# Already in C11, DAX reader lost if appears:	echo "  <uses file=\"T${M52}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	# C21
	echo "  <uses file=\"T${M22}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
# Already in C11, DAX reader lost if appears:	echo "  <uses file=\"T${M42}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	# C22
# Already in C11, DAX reader lost if appears:	echo "  <uses file=\"T${M13}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
# Already in C21, DAX reader lost if appears:	echo "  <uses file=\"T${M23}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
# Already in C12, DAX reader lost if appears:	echo "  <uses file=\"T${M32}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	echo "  <uses file=\"T${M63}\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizeqmat\"/>" >> "${WFfilename}Part2"
	# Output C11 C12 C21 C22
	echo "  <uses file=\"TC_0\" link=\"output\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizemat\"/>" >> "${WFfilename}Part2"
	echo "</job>" >> "${WFfilename}Part2"
	# Part job dependancies
	echo "<child ref=\"ID$job_id\">" >> "${WFfilename}Part3"
	for i in $Cxy_parent; do
	    echo "  <parent ref=\"ID$i\"/>" >> "${WFfilename}Part3"
	    if [ $gz -eq 1 ]; then
		echo "$job_id => $i # FINISH" >> "${GZfilename}"
	    fi
	done
	echo "</child>" >> "${WFfilename}Part3"
    else # recursive
	echo "Parents recursive: $Cxy_parent"
	eval $__parentCxy=\"'$Cxy_parent'\"
    fi
}

########################################################################## MAIN

if [ $# -lt 2 ]; then
    usage
    exit 1
fi

# Nhost, biggest size of Matrix N*N that fits on host
# nbMatrices that can be loaded in RAM, 2 (yet, 12 sums for C_0)
nbmatricesonhost=2
Nhost=$(a="$nbmatricesonhost $(free | grep Mem)" ; echo $a | awk '{printf("%d\n",int(sqrt($3/$1)))}')

case $1 in
  "-l") LVL=$2
	if [ $LVL -eq 0 ]; then
	    usage
	    exit 1
	fi
	echo Compute original matrix size from size $Nhost on host for $LVL recursions
	N=$Nhost
	while [ $LVL -ne 0 ]; do
	    N=$(( N * 2 ))
	    LVL=$(( LVL - 1 ))
	done
	LVL=$2
	Nmin=$Nhost
	;;
  "-h") echo Compute recursion level so that $nbmatricesonhost matrices fit on host with original matrices of size $2x$2
	N=$2
	LVL=0
	while [ $N -gt $Nhost ]; do
	    N=$(( N / 2 ))
	    LVL=$(( LVL + 1 ))
	done
	Nmin=$N
	checkpow=$(echo "$N ^ ($LVL+1)" | bc)
	if [ $checkpow -ne $2 ]; then
	    N=$checkpow
	    echo $2 was given as size but not power of 2. Taking N=$checkpow
	else
	    N=$2
	fi
	;;
  "-n") N=$(echo $2 | cut -f 1 -d ':')
	Nmax=$(echo $2 | cut -f 2 -d ':')
	echo Compute level until matrices are at most of size $Nmax
	if [ $Nhost -lt $Nmax ] ;then
	    echo Warning: size $Nmax is greater than size for $nbmatricesonhost matrices on host
	fi
	LVL=0
	Ntmp=$N
	while [ $Ntmp -gt $Nmax ]; do
	    Ntmp=$(( Ntmp / 2 ))
	    LVL=$(( LVL + 1 ))
	done
	Nmin=$Ntmp
	checkpow=$(echo "$N ^ ($LVL+1)" | bc)
	if [ $checkpow -ne $N ]; then
	    N=$checkpow
	    echo $2 was given as size but not power of 2. Taking N=$checkpow
	fi
	;;
    -m) LVL=$(echo $2 | cut -f 1 -d ':')
	Nmax=$(echo $2 | cut -f 2 -d ':')
	echo Compute original matrix size from size $Nmax on host and $LVL level of recursion
	if [ $Nhost -lt $Nmax ] ;then
	    echo Warning: size $Nmax is greater than size for $nbmatricesonhost matrices on host
	fi
	N=$Nmax
	while [ $LVL -ne 0 ]; do
	    N=$(( N * 2 ))
	    LVL=$(( LVL - 1 ))
	done
	LVL=$2
	Nmin=$Nmax
	;;
    *) usage
       exit 1
esac
echo "StrassenAll with NxN A*B, N=$N and recursion level $LVL"
CPU=$(cat /proc/cpuinfo | grep 'model name' | cut -f 2 -d ':' | uniq | sed "s/[ ()]/_/g")
WFfilename="$(basename $0 .sh)${CPU}_${LVL}_$N"
if [ "x$3" == "x-g" ]; then
    gz=1
    GZfilename="${WFfilename}".dot
    echo "digraph G {" > "${GZfilename}"
else
    gz=0
fi
echo "Bench for sums"
size=$N
for i in $(seq 1 $((LVL+1))); do
    size=$(( size / 2 ))
    echo -n ". lvl $i : $size x $size -> "
    tmp="scale=6 ; (0"
    idxmax=3
    for j in $(seq 1 $idxmax); do
	tmp="$tmp + $(./benchMatrixOP -s -N $size)"
    done
    tabsum[$i]=$(echo "$tmp)/$idxmax" | bc | awk '{printf "%f", $0}')
    echo ${tabsum[$i]}
done
echo -n "Bench for dgemm of size $Nmin * $Nmin -> "
tmp="scale=6 ; (0"
idxmax=3
for j in $(seq 1 $idxmax); do
    tmp="$tmp+$(./benchMatrixOP -d -N $Nmin)"
done
durationDGEMM=$(echo "$tmp)/$idxmax" | bc | awk '{printf "%f", $0}')
echo $durationDGEMM

JOBCOUNT=0
job_id=$(echo $JOBCOUNT | awk '{printf("%05d",$1);}')
JOBCOUNT=1
# Part description of job
echo "<job id=\"ID$job_id\" namespace=\"STRASSEN\" name=\"START\" version=\"1.0\" level=\"0\" runtime=\"0\">" >> "${WFfilename}Part2"
sizemat=$(echo "$N * $N * 8" | bc) # 8 bytes for a double
echo "  <uses file=\"TA_0\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizemat\"/>" >> "${WFfilename}Part2"
echo "  <uses file=\"TB_0\" link=\"input\" register=\"true\" transfer=\"true\" optional=\"false\" type=\"data\" size=\"$sizemat\"/>" >> "${WFfilename}Part2"
echo "</job>" >> "${WFfilename}Part2"

varname="" # only useful by its presence in the call. Not sure mandatory.
strassenAll 1 A B $N varname $job_id $job_id

echo "Generating DAX file ${WFfilename}"
echo '<?xml version="1.0" encoding="UTF-8"?>' > "${WFfilename}"
echo """<adag xmlns=\"local generated\" version=\"2.1\" jobCount=\"$JOBCOUNT\" fileCount=\"0\" childCount=\"${JOBCOUNT}\">""" >> "${WFfilename}"
cat "${WFfilename}Part2" "${WFfilename}Part3" >> "${WFfilename}"
echo "</adag>" >> "${WFfilename}"
rm "${WFfilename}Part2" "${WFfilename}Part3"
if [ $gz -eq 1 ]; then
    echo "}" >> "${GZfilename}"
    while read LINE; do
	if [ $(echo $LINE | grep '=' | wc -l) -eq 0 ]; then
	    echo $LINE
	else
	    echo $LINE | awk '{printf("\"%05d\" -> \"%05d\" # %s\n",$3,$1,$5)}'
	fi
    done < "${GZfilename}"  > 2"${GZfilename}"
    #    cp "${GZfilename}" test
    mv 2"${GZfilename}" "${GZfilename}"
    if [ -x $(which dot | awk '{print $1}') ]; then
	gzout=${WFfilename}.svg
	echo Generating picture $gzout with graphviz
	dot -Tsvg -o$gzout "${GZfilename}"
    fi
fi
