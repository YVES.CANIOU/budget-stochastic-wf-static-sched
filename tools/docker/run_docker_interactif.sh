#! /bin/bash
# Observation : here we set the directory where is our simulator as a volume 
# because we need to modified and test it.
# Another solution, if we don't need to modified the simulator, would be directly
# download the simulator in the Dockerfile

# set variable 
image=bmairesse-simgrid:v3.15
volume=/home/bmairesse/budget-stochastic-wf-sched #the directory where the simulator is
mount_point=/home/simulator
exe=bash

# run 
docker run -it --rm --volume $volume:$mount_point $image $exe
