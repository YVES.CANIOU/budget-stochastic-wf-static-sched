#ifndef FTL_MULT_LAST_TASK_LVL_H
#define FTL_MULT_LAST_TASK_LVL_H

#include "workflow.h"

SD_task_t getLastTskLvl(std::map<SD_task_t, unsigned int> &eftPTsk,
                               const std::list<SD_task_t> &lvl);

void algo_offline_ftl_mult_last_task_lvl(
    Workflow &wf, SD_task_t ordoT[], std::map<SD_task_t, sg_host_t> &m,
    double deadline, double budget, double cagnotte,
    std::map<sg_host_t, HostCost> mhc);
#endif