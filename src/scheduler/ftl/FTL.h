#ifndef FTL_H
#define FTL_H

#include "hostCost.h"
#include "workflow.h"

template <typename T>
bool contains(const std::list<T> &listOfElements, const T &element);

sg_host_t SD_task_get_best_host_time(const SD_task_t &task,
                                     const double taskBudg, double &cagnotte,
                                     const double finishTime,
                                     std::map<sg_host_t, HostCost> &mCostPHost);

void algo_offline_ftl(Workflow &wf, SD_task_t ordoT[],
                      std::map<SD_task_t, sg_host_t> &m, double budget,
                      double cagnotte, std::map<sg_host_t, HostCost> mhc,
                      std::map<SD_task_t, unsigned int> &eftPTsk,
                      std::map<SD_task_t, double> &newBudgPTsk);
#endif