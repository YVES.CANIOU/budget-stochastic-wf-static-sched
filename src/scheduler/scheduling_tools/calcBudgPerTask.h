#ifndef CALC_BUDG_PER_TASK_H
#define CALC_BUDG_PER_TASK_H

// explosion des limites sup des doubles --> voir si gmp n'aurait pas un type de
// remplacement

#include "workflow.h"

void getDataTsk(const Workflow &wf, std::map<SD_task_t, double> &WmaxPTask,
                std::map<SD_task_t, double> &transfPTask);

double getWorkMaxWF(const Workflow &wf, std::map<SD_task_t, double> &WmaxPTask);

double getTransfWF(const Workflow &wf,
                   std::map<SD_task_t, double> &transfPTask);

void getDataWF(const Workflow &wf, double &Wmax, double &transfMax,
               std::map<SD_task_t, double> &WmaxPTask,
               std::map<SD_task_t, double> &transfPTask);

void calcBudgPTsk(Workflow &wf, double budget, double bw, double speed_m,
                  std::map<SD_task_t, double> &budgPTsk);
#endif
