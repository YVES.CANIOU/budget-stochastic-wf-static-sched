#include "simgrid/simdag.h"
#include "xbt.h"
#include <iostream>
#include <list>
#include <map>

#include "calcBudgPerTask.h"
#include "toolsLevel.h"
#include "toolsSched.h"
#include "workflow.h"

/**
 * @brief Display all path
 * @param listPath : list of all wf's path
 */
void displayLevels(
    const std::map<unsigned int, std::list<SD_task_t>> &listTaskPLvl) {
  std::cout << "-----Number of Level : " << listTaskPLvl.size() << std::endl;
  unsigned int numList = 0;
  for (const auto listTask : listTaskPLvl) {
    std::cout << "-----The list : " << listTask.first << std::endl
              << "----------Contain the following tasks : " << std::flush;

    for (const auto task : listTask.second) {
      std::cout << SD_task_get_name(task) << " - " << std::flush;
    }
    std::cout << std::endl;
    numList++;
  }
}

/**
 * @brief calcul all level of the wf from end to root, this function is recursif
 * @param lvlPTsk : map between task and it level
 * @param T : the current task
 * @param lvl : the current lvl of the task
 * @return a vector of path list
 */
void getTskLvlRec(std::map<SD_task_t, unsigned int> &lvlPTsk, SD_task_t T,
                  unsigned int lvl) {
  // the first task of the graph
  if (strcmp(SD_task_get_name(T), "root") == 0) {
    lvlPTsk[T] = std::max(lvlPTsk[T], lvl + 1);
  }

  else {
    auto search = lvlPTsk.find(T);
    // if we find the task then we have already pass in the taks and as
    // consequently we need to test if the level is > lvlPTsk[T] to know if we
    // need to repass in this taks
    if ((search != lvlPTsk.end()) && !(lvlPTsk[T] < lvl)) {
      return;
    } else { // nothing find, we also need to process all parent
      lvlPTsk[T] = lvl;
    }

    // in the case the taks is end or the grdChild of root, this task hasn't a
    // computable parent (because they are just tranfere taks) and we need to go
    // directly to the grand parents

    xbt_dynar_t dynarUse = SD_task_get_parents(T);
    // if a task hasn't computable parents then this task is a root grdChild
    if ((strcmp(SD_task_get_name(T), "end") == 0) ||
        (minOneComputableTask(dynarUse))) {

      xbt_dynar_t grdParents = xbt_dynar_new(sizeof(SD_task_t), NULL);

      // process all child and grand child
      size_t cursorParent, cursorGrdParent;
      SD_task_t parent, grdParent;

      // process parents
      xbt_dynar_foreach(dynarUse, cursorParent, parent) {
        xbt_dynar_t thisGrdParents = SD_task_get_parents(parent);

        // process grd parents
        xbt_dynar_foreach(thisGrdParents, cursorGrdParent, grdParent) {
          if (isTaskComput(grdParent)) {

            xbt_dynar_push(grdParents, &grdParent);
          }
        } // process grd parents
        xbt_dynar_free_container(&thisGrdParents);
      } // process parents
      xbt_dynar_free_container(&dynarUse);
      dynarUse = grdParents;

    } // if end or grdChild root

    // here we have the right dynar and go to process all task in it
    unsigned int cursor = 0;
    SD_task_t task;
    xbt_dynar_foreach(dynarUse, cursor, task) {
      if (isTaskComput(task)) {
        getTskLvlRec(lvlPTsk, task, lvl + 1);
      }
    }
    xbt_dynar_free_container(&dynarUse);
  }
} // function getLvlTskRec

/**
 * @brief calcul all level for each wf's task
 * @param wf : a workflow
 * @return a map between a level and a list of taks
 */
std::map<unsigned int, std::list<SD_task_t>> getTskLvl(const Workflow &wf) {
  std::map<unsigned int, std::list<SD_task_t>> listTskPLvl;

  std::map<SD_task_t, unsigned int> lvlPTsk;
  // variables use in the loop
  size_t cursor = 0;
  SD_task_t task;
  // search the last simgrid task to find paths since it
  xbt_dynar_foreach(getDagWf(wf), cursor, task) {
    if (strcmp(SD_task_get_name(task), "end") == 0) {
      getTskLvlRec(lvlPTsk, task, 0);
    }
  }

  // process the list to make the list of taks per level
  for (const auto element : lvlPTsk) {
    listTskPLvl[element.second].push_back(element.first);
  }

  return listTskPLvl;
}
