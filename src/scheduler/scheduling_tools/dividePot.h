#ifndef DIVIDE_POT_H
#define DIVIDE_POT_H

#include "workflow.h"

void divPotNBChildren(const Workflow &wf, const SD_task_t &task,
                      const double pot, std::map<SD_task_t, double> &budgPTsk);

void divPotWeighChildPath(const Workflow &wf, const SD_task_t &task,
                          const double pot,
                          std::map<SD_task_t, double> &budgPTsk);

void divPotDescendantChild(const Workflow &wf, const SD_task_t &task,
                           const double pot,
                           std::map<SD_task_t, double> &budgPTsk);

void notDivPot(const Workflow &wf, const SD_task_t &task, const double pot,
               std::map<SD_task_t, double> &budgPTsk);
#endif