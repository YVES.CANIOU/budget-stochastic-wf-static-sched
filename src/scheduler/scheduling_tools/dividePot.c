#include "simgrid/simdag.h"
#include "xbt.h"
#include <iostream>
#include <list>
#include <map>

#include "dividePot.h"
#include "toolsPath.h"
#include "toolsSched.h"
#include "workflow.h"

/**
 * @brief divide the budget leftover of a task to all it children in function of
 * the number of children. For example, if a task has n children, then each
 * children going to have their budget more pot/n.
 * @param task: the task to wich the reminder is distributed
 * @param pot: the budget to divide
 * @param budgPTsk : the budget per task
 */
void divPotNBChildren(const Workflow &wf, const SD_task_t &task,
                      const double pot, std::map<SD_task_t, double> &budgPTsk) {
  xbt_dynar_t children = SD_task_get_children(task);
  // test if the task have child
  if (!xbt_dynar_is_empty(children)) {
    SD_task_t child;
    unsigned int cursor = 0;
    // first, count the number of computable child
    unsigned int nbCHildren = 0;
    xbt_dynar_foreach(children, cursor, child) {
      if (isTaskComput(child)) {
        nbCHildren++;
      }
    }
    // second, alloucation of the budget leftover
    const double potPTsk = pot / nbCHildren;
    xbt_dynar_foreach(children, cursor, child) {
      if (isTaskComput(child)) {
        budgPTsk[child] += potPTsk;
      }
    }
  }
  // clean the container
  xbt_dynar_free_container(&children);
}

/**
 * @brief divide the budget leftover of a task to all it children in function of
 * the weight of all child's path. In fact, we take all path beetween a child
 * and Tend and we count the amount of work for every path and we make the sum
 * of all (paths of every children), and as a result every children going to
 * have their budget more pot * (work of all paths of the child / sum of all
 * paths of children)
 * @param wf: the workflow
 * @param task: the task to wich the reminder is distributed
 * @param pot: the budget to divide
 * @param budgPTsk : the budget per task
 */
void divPotWeighChildPath(const Workflow &wf, const SD_task_t &task,
                          const double pot,
                          std::map<SD_task_t, double> &budgPTsk) {
  xbt_dynar_t children = SD_task_get_children(task);
  // test if the task have child
  if (!xbt_dynar_is_empty(children)) {
    std::map<SD_task_t, double> weightPathChildren;

    SD_task_t child;
    unsigned int cursor = 0;
    // process children to get the amount of work of paths
    xbt_dynar_foreach(children, cursor, child) {
      if (isTaskComput(child)) {
        // init the weight
        weightPathChildren[child] = 0;
        // get paths from the child
        std::vector<std::list<SD_task_t>> arrPath = getPathsFrom(wf, child);
        // get the total weigh for the paths
        for (const auto &path : arrPath) {
          for (const auto &task : path) {
            // amount of the task
            weightPathChildren[child] += SD_task_get_amount(task);
            // get the transfert task
            weightPathChildren[child] += getAmountInOutData(task);
          }
        }
      }
    }
    // calcul the total of amount of work
    double totalWeigh = 0;
    for (const auto ele : weightPathChildren) {
      totalWeigh += ele.second;
    }
    // divide the pot beetween all children
    xbt_dynar_foreach(children, cursor, child) {
      if (isTaskComput(child)) {
        budgPTsk[child] += pot * (weightPathChildren[child] / totalWeigh);
      }
    }
    // clean the container
    xbt_dynar_free_container(&children);
  }
}

/**
 * @brief divide the budget leftover of a task to all it children in function of
 * the weight of all child's descendant. In fact, for every children we count
 * the amount of work for all it descendant, we make the sum of all and every
 * taks going to have their budget more pot * (work of descendant's child / work
 * of descendant's children)
 * @param wf: the workflow
 * @param task: the task to wich the reminder is distributed
 * @param pot: the budget to divide
 * @param budgPTsk : the budget per task
 */
void divPotDescendantChild(const Workflow &wf, const SD_task_t &task,
                           const double pot,
                           std::map<SD_task_t, double> &budgPTsk) {
  xbt_dynar_t children = SD_task_get_children(task);
  // test if the task have child
  if (!xbt_dynar_is_empty(children)) {
    std::map<SD_task_t, double> weightDescChild;

    SD_task_t child;
    unsigned int cursor = 0;

    // process children to get the amount of work of paths
    xbt_dynar_foreach(children, cursor, child) {
      if (isTaskComput(child)) {
        // use to know if we have already count the data of a task
        std::map<SD_task_t, bool> alreadyCount;
        // init the weight
        weightDescChild[child] = 0;
        // get paths from the child
        std::vector<std::list<SD_task_t>> arrPath = getPathsFrom(wf, child);
        // get the total weigh for the paths
        for (const auto &path : arrPath) {
          for (const auto &task : path) {
            // if the task has never been counted
            if (alreadyCount.find(task) == alreadyCount.end()) {
              alreadyCount[task] = false;
            }
            if (!alreadyCount[task]) {
              // amount of the task
              weightDescChild[child] += SD_task_get_amount(task);
              // get the transfert task
              weightDescChild[child] += getAmountInOutData(task);

              alreadyCount[task] = true;
            }
          }
        }
      }
    }
    // calcul the total of amount of work
    double totalWeigh = 0;
    for (const auto ele : weightDescChild) {
      totalWeigh += ele.second;
    }
    // divide the pot beetween all children
    xbt_dynar_foreach(children, cursor, child) {
      if (isTaskComput(child)) {
        budgPTsk[child] += pot * (weightDescChild[child] / totalWeigh);
      }
    }
    // clean the container
    xbt_dynar_free_container(&children);
  }
}

/**
 * @brief divide the budget leftover of a task to all it children in function of
 * the weight of all child's descendant. In fact, for every children we count
 * the amount of work for all it descendant, we make the sum of all and every
 * taks going to have their budget more pot * (work of descendant's child / work
 * of descendant's children)
 * @param wf: the workflow
 * @param task: the task to wich the reminder is distributed
 * @param pot: the budget to divide
 * @param budgPTsk : the budget per task
 */
void notDivPot(const Workflow &wf, const SD_task_t &task, const double pot,
               std::map<SD_task_t, double> &budgPTsk) {}