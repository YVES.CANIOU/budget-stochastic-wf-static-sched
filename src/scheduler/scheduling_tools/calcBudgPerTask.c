#include "simgrid/simdag.h"
#include "xbt.h"
#include <iostream>
#include <list>
#include <map>

#include "toolsSched.h"
#include "workflow.h"

/**
 * @brief calcul the amount of transfered data and the amount max of work per
 * task
 * @param wf : the workflow
 */
void getDataTsk(const Workflow &wf, std::map<SD_task_t, double> &WmaxPTask,
                std::map<SD_task_t, double> &transfPTask) {
  SD_task_t task;
  InfoStochTask ist;
  unsigned int cursor;
  int i;

  xbt_dynar_foreach(getDagWf(wf), cursor, task) {
    ist = getInfoStochTaskWf(wf, task);
    WmaxPTask[task] = getMeanIst(ist) + getSdIst(ist);

    double ficIn = 0;
    if (isTaskComput(task)) {
      // fichiers in
      xbt_dynar_t parents = SD_task_get_parents(task);

      if (!xbt_dynar_is_empty(parents)) {
        /* compute last_data_available */
        SD_task_t parent;

        xbt_dynar_foreach(parents, i, parent) {
          /* normal case */
          if (SD_task_get_kind(parent) == SD_TASK_COMM_E2E) {

            if (SD_task_get_amount(parent) <= 1e-6) {
              ficIn += 0;
            } else {
              ficIn += SD_task_get_amount(parent);
            }
          }
        }
      }
      xbt_dynar_free(&parents);

      // fichier out
      xbt_dynar_t children = SD_task_get_children(task);
      double ficOut = 0;
      if (!xbt_dynar_is_empty(children)) {
        /* compute last_data_available */
        SD_task_t child;

        xbt_dynar_foreach(children, i, child) {
          /* normal case */
          if (SD_task_get_kind(child) == SD_TASK_COMM_E2E) {

            if (SD_task_get_amount(child) <= 1e-6) {
              ficOut += 0;
            } else {
              ficOut += SD_task_get_amount(child);
            }
          }
        }
      }
      xbt_dynar_free(&children);
      transfPTask[task] = ficIn + ficOut;
    }
  }
}

/**
 * @brief calcul the amount of work max for the wf
 * @param wf : the workflow
 * @param WmaxPTask : the map between a task and it amount max of work
 * @return the value of the amount of work max for the wf
 */
double getWorkMaxWF(const Workflow &wf,
                    std::map<SD_task_t, double> &WmaxPTask) {
  double Wmax = 0;
  SD_task_t task;
  InfoStochTask ist;
  unsigned int cursor;
  xbt_dynar_t dag = getDagWf(wf);
  // process the graph
  xbt_dynar_foreach(dag, cursor, task) {

    if (isTaskComput(task)) {
      Wmax += WmaxPTask[task];
    }
  } // end foreach task
  return Wmax;
}

/**
 * @brief calcul the amount of tranfere data for the wf
 * @param wf : the workflow
 * @param transfPTask :  the map between a task and it amount of transfere data
 * @return the value of the amount of transfere data for the wf
 */
double getTransfWF(const Workflow &wf,
                   std::map<SD_task_t, double> &transfPTask) {
  double transfMax = 0;
  SD_task_t task;
  InfoStochTask ist;
  unsigned int cursor;
  xbt_dynar_t dag = getDagWf(wf);
  // process the graph
  xbt_dynar_foreach(dag, cursor, task) {

    if (isTaskComput(task)) {
      transfMax += transfPTask[task];
    }
  } // end foreach task
  return transfMax;
}

void getDataWF(const Workflow &wf, double &Wmax, double &transfMax,
               std::map<SD_task_t, double> &WmaxPTask,
               std::map<SD_task_t, double> &transfPTask) {
  SD_task_t task;
  unsigned int cursor;
  // process the graph
  xbt_dynar_foreach(getDagWf(wf), cursor, task) {
    if (isTaskComput(task)) {
      transfMax += transfPTask[task];
      Wmax += WmaxPTask[task];
    }
  }
}

/**
 * @brief divide the budget between all task
 *
 * @param wf : the workflow
 * @param budget : the budget
 * @param bw : the bandwigth
 * @param speed_m : the mean speed off all VMs
 * @param budgPTsk : the map between a task and it budget
 *
 * @return the map between a task and it budget
 */
void calcBudgPTsk(Workflow &wf, double budget, double bw, double speed_m,
                  std::map<SD_task_t, double> &budgPTsk) {

  std::cout << "check : budget à distribuer : " << budget << "\n";

  /****************************************************************************
   ***** Tasks data
   ****************************************************************************/
  // get the amount max of work per task
  std::map<SD_task_t, double> WmaxPTask;
  // get the amount of transfere per task
  std::map<SD_task_t, double> transfPTask;

  getDataTsk(wf, WmaxPTask, transfPTask);

  /****************************************************************************
  ***** WF data
  ****************************************************************************/
  // calcul amount of work max
  double Wmax = 0;
  // transfere data in th wf
  double transfMax = 0;

  getDataWF(wf, Wmax, transfMax, WmaxPTask, transfPTask);

  //***************************************************************************
  unsigned int cursor;
  SD_task_t task;
  xbt_dynar_t dag = getDagWf(wf);

  // wf's time
  const double timeWF = Wmax / speed_m + transfMax / bw;
  xbt_dynar_foreach(dag, cursor, task) {
    if (isTaskComput(task)) {

      // task's time
      const double timeTask =
          WmaxPTask[task] / speed_m + transfPTask[task] / bw;
      // task's budget
      budgPTsk[task] = budget * timeTask / timeWF;
    }
  }
}
