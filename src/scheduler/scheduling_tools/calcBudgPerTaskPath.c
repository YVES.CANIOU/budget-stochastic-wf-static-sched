#include "simgrid/simdag.h"
#include "xbt.h"
#include <list>
#include <map>

#include "calcBudgPerTask.h"
#include "workflow.h"

/**
 * @brief calcule the amount of transfere data and the amount max of work per
 * level AND the amount max of work and the amount of transfere data of all
 * level
 *
 * @param listPath : the list of all path
 * @param WmaxPPath: the map between a path and it amount max of work
 * @param transfPPath : the map between a path and it amount of transfere data
 * @param WmaxPath: the total amount of work for all path
 * @param transfPath: the total amoutn of transfere for all path
 * @param WmaxPTask : the map between a task and it amount max of work
 * @param transfPTask :  the map between a task and it amount of transfere data
 */
void getDataPath(const std::vector<std::list<SD_task_t>> listPath,
                 std::map<std::list<SD_task_t>, double> &WmaxPPath,
                 std::map<std::list<SD_task_t>, double> &transfPPath,
                 double &WmaxPath, double &transfPath,
                 std::map<SD_task_t, double> &WmaxPTask,
                 std::map<SD_task_t, double> &transfPTask) {

  for (const auto &path : listPath) {
    // initialize the path's amount of work
    WmaxPPath[path] = 0;

    for (const auto &task : path) {
      // add to the path's amount of work the task's amount of work
      WmaxPPath[path] += WmaxPTask[task];
      // add to the path's amount of work the task's amount of work
      transfPPath[path] += transfPTask[task];
    }
    WmaxPath += WmaxPPath[path];
    transfPath += transfPPath[path];
  }
}

/**
 * @brief divide the budget between all task according to all path
 *
 * @param wf : the workflow
 * @param budget : the budget
 * @param bw : the bandwigth
 * @param speed_m : the mean speed off all VMs
 * @param budgPTsk : the map between a task and it budget
 * @param listPath : the list of all paths
 *
 * @return the map between a task and it budget
 */
void calcBudgPTskPath(Workflow &wf, double budget, double bw, double speed_m,
                      std::map<SD_task_t, double> &budgPTsk,
                      std::vector<std::list<SD_task_t>> listPath) {

  /****************************************************************************
   ***** Init the buget per task to 0
   ****************************************************************************/
  unsigned int cursor;
  SD_task_t task;
  InfoStochTask ist;
  xbt_dynar_t dag = getDagWf(wf);
  // init the task's budget to 0
  xbt_dynar_foreach(dag, cursor, task) {
    if (isTaskComput(task)) {
      budgPTsk[task] = 0;
    }
  }

  /****************************************************************************
  ***** Tasks data
  ****************************************************************************/
  // get the amount max of work per task
  std::map<SD_task_t, double> WmaxPTask;
  // get the amount of transfere per task
  std::map<SD_task_t, double> transfPTask;

  getDataTsk(wf, WmaxPTask, transfPTask);
  /****************************************************************************
  ***** Paths data
  ****************************************************************************/
  // get the amount max of work per task
  std::map<std::list<SD_task_t>, double> WmaxPPath;
  // get the amount of transfere per task
  std::map<std::list<SD_task_t>, double> transfPPath;

  // get the total amount of work of all path
  double WmaxPath = 0;
  // get the total amount of tranfere data of all path
  double transfPath = 0;

  getDataPath(listPath, WmaxPPath, transfPPath, WmaxPath, transfPath, WmaxPTask,
              transfPTask);
  /****************************************************************************
  ***** Divide the budget according to the path
  ****************************************************************************/
  // all path's time
  const double timeAllPath = WmaxPath / speed_m + transfPath / bw;
  for (const auto &path : listPath) {
    // path's time
    const double timePath = WmaxPPath[path] / speed_m + transfPPath[path] / bw;
    // path's budget
    const double budgPath = budget * timePath / timeAllPath;

    for (const auto &task : path) {
      // task's time
      const double timeTask =
          WmaxPTask[task] / speed_m + transfPTask[task] / bw;
      // task's budget
      budgPTsk[task] += budgPath * timeTask / timePath;
    }
  }
}
