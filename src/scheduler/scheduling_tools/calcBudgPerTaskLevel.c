#include "simgrid/simdag.h"
#include "xbt.h"
#include <list>
#include <map>

#include "calcBudgPerTask.h"
#include "calcBudgPerTaskLevel.h"
#include "toolsSched.h"
#include "workflow.h"

/**
 * @brief calcule the amount of transfere data and the amount max of work per
 * level AND the amount max of work and the amount of transfere data of all
 * level
 *
 * @param listTaskPLvl : the list of all level
 * @param WmaxPLvl: the map between a level and it amount of work
 * @param transfPLvl : the map between a level and it amount of transfere data
 * @param WmaxLvl: the total amount of work for all Lvl
 * @param transfLvl: the total amoutn of transfere for all LVl
 * @param WmaxPTask : the map between a task and it amount max of work
 * @param transfPTask :  the map between a task and it amount of transfere data
 */
void getDataLvl(const std::map<unsigned int, std::list<SD_task_t>> listTaskPLvl,
                std::map<unsigned int, double> &WmaxPLvl,
                std::map<unsigned int, double> &transfPLvl, double &WmaxLvl,
                double &transfLvl, std::map<SD_task_t, double> &WmaxPTask,
                std::map<SD_task_t, double> &transfPTask) {

  for (const auto lvl : listTaskPLvl) {
    // initialize the path's amount of work
    WmaxPLvl[lvl.first] = 0;

    for (const auto &task : lvl.second) {
      // add to the path's amount of work the task's amount of work
      WmaxPLvl[lvl.first] += WmaxPTask[task];
      // add to the path's amount of work the task's amount of work
      transfPLvl[lvl.first] += transfPTask[task];
    }
    WmaxLvl += WmaxPLvl[lvl.first];
    transfLvl += transfPLvl[lvl.first];
  }
}

/**
 * @brief divide the budget between all task according to all level
 *
 * @param wf : the workflow
 * @param budget : the budget
 * @param bw : the bandwigth
 * @param speed_m : the mean speed off all VMs
 * @param budgPTsk : the map between a task and it budget
 * @param listTaskPLvl : the map between a level and a list of task
 *
 * @return the map between a task and it budget
 */
void calcBudgPTskLvl(
    Workflow &wf, double budget, double bw, double speed_m,
    std::map<SD_task_t, double> &budgPTsk,
    const std::map<unsigned int, std::list<SD_task_t>> listTaskPLvl) {

  /****************************************************************************
   ***** Init the buget per task to 0
   ****************************************************************************/
  unsigned int cursor;
  SD_task_t task;
  InfoStochTask ist;
  // init the task's budget to 0
  xbt_dynar_foreach(getDagWf(wf), cursor, task) {
    if (isTaskComput(task)) {
      budgPTsk[task] = 0;
    }
  }

  /****************************************************************************
  ***** Tasks data
  ****************************************************************************/
  // get the amount max of work per task
  std::map<SD_task_t, double> WmaxPTask;
  // get the amount of transfere per task
  std::map<SD_task_t, double> transfPTask;

  getDataTsk(wf, WmaxPTask, transfPTask);
  /****************************************************************************
  ***** Level data
  ****************************************************************************/
  // get the amount max of work per task
  std::map<unsigned int, double> WmaxPLvl;

  // get the amount of transfere per task
  std::map<unsigned int, double> transfPLvl;

  // get the total amount of work of all path
  double WmaxLvl = 0;
  // get the total amount of tranfere data of all path
  double transfLvl = 0;

  getDataLvl(listTaskPLvl, WmaxPLvl, transfPLvl, WmaxLvl, transfLvl, WmaxPTask,
             transfPTask);

  /****************************************************************************
  ***** Divide the budget according to the path
  ****************************************************************************/
  // all path's time
  const double timeAllLvl = WmaxLvl / speed_m + transfLvl / bw;
  for (const auto lvl : listTaskPLvl) {
    // path's time
    const double timeLvl =
        WmaxPLvl[lvl.first] / speed_m + transfPLvl[lvl.first] / bw;
    // path's budget
    const double budgLvl = budget * timeLvl / timeAllLvl;

    for (const auto task : lvl.second) {
      // task's time
      const double timeTask =
          WmaxPTask[task] / speed_m + transfPTask[task] / bw;
      // task's budget
      budgPTsk[task] += budgLvl * timeTask / timeLvl;
    }
  }
}