#ifndef ALGO_CRITICAL_GREEDY_TOTAL_FAIR_H
#define ALGO_CRITICAL_GREEDY_TOTAL_FAIR_H

// explosion des limites sup des doubles --> voir si gmp n'aurait pas un type de
// remplacement

#include "hostCost.h"
#include "workflow.h"

double finish_on_at_CG_total_fair(SD_task_t task, sg_host_t host);

sg_host_t SD_task_get_best_host_CG_total_fair(SD_task_t task,
                                              std::map<sg_host_t, HostCost> mhc,
                                              double &timeCalc, double gbl);

void algo_offline_CG_total_fair(Workflow &wf, SD_task_t ordoT[],
                                       std::map<SD_task_t, sg_host_t> &m,
                                       double deadline, double budget,
                                       double cagnotte,
                                       std::map<sg_host_t, HostCost> mhc,
                                       double Cmin, double Cmax);

#endif
