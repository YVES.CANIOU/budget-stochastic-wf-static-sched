#include <iostream>
#include <limits>
#include <list>
#include <map>
#include <set>
#include <sys/wait.h>

#include "HEFT.h"
#include "algo_arabnejad.h"
#include "calcBudgPerTask.h"
#include "hostCost.h"
#include "outputFiles.h"
#include "toolsSched.h"
#include "workflow.h"

//==========================================
// outils
//-----------
// sg_host_t SD_task_get_best_host_arabnejad(SD_task_t task,
// std::map<sg_host_t, HostCost> mhc, std::map<SD_task_t, double> budgPTsk,
// double cagnotte, double &timeCalc, double &cagnotteFin){

sg_host_t SD_task_get_best_host_arabnejad(SD_task_t task, BoTlvl &BoTAct,
                                          std::map<sg_host_t, HostCost> mhc,
                                          std::map<int, BoTlvl> &mLvlSet,
                                          double &selected_cost) {

  std::cout << "début algo : tâche : " << SD_task_get_name(task) << ", "
            << ", autorisé :" << get_subBudget_BoTlvl(BoTAct) << "\n";
  sg_host_t best_host;

  // if(SD_task_get_state(task) == SD_SCHEDULABLE){
  if (1) {
    std::cout << "cette tâche est schedulable\n";

    sg_host_t *hosts = sg_host_list();
    int nhosts = sg_host_count();
    double bw = sg_host_route_bandwidth(hosts[0], hosts[1]);
    double cHor = getHC_hC(mhc[hosts[0]]);
    double costTmp = 0.;

    // initialisation : hôte le moins cher
    for (int i = 1; i < nhosts; i++) {
      // si moins cher, devient le best host
      if (getHC_hC(mhc[hosts[i]]) <= cHor) {
        cHor = getHC_hC(mhc[hosts[i]]);
        best_host = hosts[i];
      }
    }

    double cost = 0;
    double timeCalcTps = 0;
    double budgTsk = get_subBudget_BoTlvl(BoTAct);

    // calcul du temps
    double ECT = finish_on_at(task, best_host);

    // calcul du coût
    get_cost_data_prec_newVM(task, mhc);

    // calcul du premier cost
    // if ( getHC_occTime(mhc[best_host]) <= 1e-6 ) {
    if (sg_host_get_available_at(best_host) <= 1e-6) {
      //    std::cout <<"nouvel host \n";
      // le vrai temps de calcul = temps de téléchargement des données + temps
      // de calcul
      timeCalcTps = get_amount_data_needed(task) / bw +
                    SD_task_get_amount(task) / sg_host_speed(best_host);
      // on ajoute au prix le coût lié aux temps de transferts pour les machines
      // abritant les parents
      costTmp += get_cost_data_prec_newVM(task, mhc);
    } else {
      //    std::cout <<"vieil host\n";
      //    timeCalcTps = ECT - getHC_occTime(mhc[best_host]);
      timeCalcTps = ECT - sg_host_get_available_at(best_host);
      //    std::cout <<"origine calc : " <<getHC_occTime(mhc[best_host])
      //    <<"\n"; std::cout <<"temps calc : " <<timeCalcTps <<"\n";
    } // fin calcul du premier cost
    costTmp += timeCalcTps * getHC_hC(mhc[best_host]);

    // màj des valeurs recherchées

    //=================================

    //--------------------------------------------------------------
    // we won't test multiple times the same type of empty VM
    //--------------------------------------------------------
    int aTester = 0;
    int valVM[3] = {0, 0, 0};
    //--------------------------------------------------------------

    //  Calcul de ECTmax, ECTmin, Cbest
    double ECTmax = -1;
    double ECTmin = std::numeric_limits<double>::max();
    double Cbest = std::numeric_limits<double>::max();

    for (int i = 1; i < nhosts; i++) {

      // si la machine est déjà utilisée, faire le test
      if (sg_host_get_last_scheduled_task(hosts[i]) != NULL) {
        // std::cout<< "host vieux\n";
        aTester = 1;
      }

      // si la machine est neuve, n'en prendre qu'une par type de VM
      else {

        // get the type of VM
        const char *name = sg_host_get_name(hosts[i]);
        if (strlen(name) >= 3) {
          if (name[2] == '1') {
            if (valVM[0] != 2) {
              valVM[0] = 1;
              aTester = 1;
            }
          }
          if (name[2] == '2') {
            if (valVM[1] != 2) {
              valVM[1] = 1;
              aTester = 1;
            }
          }
          if (name[2] == '3') {
            if (valVM[2] != 2) {
              valVM[2] = 1;
              aTester = 1;
            }
          }
        } // end of type of vm

      } // end of if host is new

      if (aTester == 1) {

        double ECT = finish_on_at(task, hosts[i]);

        // calcul du coût induit par ce placement :
        //    timeCalcTps = EFT - sg_host_get_available_at(hosts[i]);
        // if ( getHC_occTime(mhc[hosts[i]]) <= 1e-6 ) {
        if (sg_host_get_available_at(hosts[i]) <= 1e-6) {
          // std::cout <<"nouvel host \n";
          timeCalcTps = get_amount_data_needed(task) / bw +
                        SD_task_get_amount(task) / sg_host_speed(hosts[i]);
          cost = get_cost_data_prec_newVM(task, mhc);
        } else {
          // std::cout <<"vieil host\n";
          // timeCalcTps = ECT - getHC_occTime(mhc[hosts[i]]);
          timeCalcTps = ECT - sg_host_get_available_at(hosts[i]);
          cost = 0.;
        }
        cost += timeCalcTps * getHC_hC(mhc[hosts[i]]);

        // récup de ECTmax
        if (ECT >= ECTmax) {
          ECTmax = ECT;
        }
        // récup de ECTmin
        if (ECT <= ECTmin) {
          ECTmin = ECT;
        }
        // récup de Cbest
        if (cost <= Cbest) {
          Cbest = cost;
        }

        //--------------------------------------
        // remise à zéro des variables de vérif
        int iValVM;
        for (iValVM = 0; iValVM < 3; iValVM++) {
          if (valVM[iValVM] == 1) {
            valVM[iValVM] = 2;
          }
        }
        aTester = 0;
        //--------------------------------------
      }
    }

    //====================================================
    // sélection de l'host
    //---------------------
    // remise à zéro de quelques variables
    cost = 0;
    timeCalcTps = 0;
    // calcul du premier cost
    //  if ( getHC_occTime(mhc[best_host]) <= 1e-6 ) {
    if (sg_host_get_available_at(best_host) <= 1e-6) {
      //    std::cout <<"nouvel host \n";
      // le vrai temps de calcul = temps de téléchargement des données + temps
      // de calcul
      timeCalcTps = get_amount_data_needed(task) / bw +
                    SD_task_get_amount(task) / sg_host_speed(best_host);
      // on ajoute au prix le coût lié aux temps de transferts pour les machines
      // abritant les parents
      costTmp += get_cost_data_prec_newVM(task, mhc);
    } else {
      //    std::cout <<"vieil host\n";
      // timeCalcTps = ECT - getHC_occTime(mhc[best_host]);
      timeCalcTps = ECT - sg_host_get_available_at(best_host);
      //    std::cout <<"origine calc : " <<getHC_occTime(mhc[best_host])
      //    <<"\n"; std::cout <<"temps calc : " <<timeCalcTps <<"\n";
    } // fin calcul du premier cost
    costTmp += timeCalcTps * getHC_hC(mhc[best_host]);

    // màj des valeurs recherchées

    //=================================

    //--------------------------------------------------------------
    // we won't test multiple times the same type of empty VM
    //--------------------------------------------------------
    aTester = 0;
    valVM[0] = 0;
    valVM[1] = 0;
    valVM[2] = 0;

    //--------------------------------------------------------------

    double TheurBest = -1;

    // recherche du meilleur host

    for (int i = 1; i < nhosts; i++) {
      // si la machine est déjà utilisée, faire le test
      if (sg_host_get_last_scheduled_task(hosts[i]) != NULL) {
        // std::cout<< "host vieux\n";
        aTester = 1;
      }

      // si la machine est neuve, n'en prendre qu'une par type de VM
      else {

        // get the type of VM
        const char *name = sg_host_get_name(hosts[i]);
        if (strlen(name) >= 3) {
          if (name[2] == '1') {
            if (valVM[0] != 2) {
              valVM[0] = 1;
              aTester = 1;
            }
          }
          if (name[2] == '2') {
            if (valVM[1] != 2) {
              valVM[1] = 1;
              aTester = 1;
            }
          }
          if (name[2] == '3') {
            if (valVM[2] != 2) {
              valVM[2] = 1;
              aTester = 1;
            }
          }
        } // end of type of vm

      } // end of if host is new
      //    aTester = 1;
      if (aTester == 1) {
        std::cout << "machine : " << sg_host_get_name(hosts[i]) << "\n";

        double ECT = finish_on_at(task, hosts[i]);

        // calcul du coût induit par ce placement :
        //    timeCalcTps = EFT - sg_host_get_available_at(hosts[i]);
        // if ( getHC_occTime(mhc[hosts[i]]) <= 1e-6 ) {
        if (sg_host_get_available_at(hosts[i]) <= 1e-6) {
          // std::cout <<"nouvel host \n";
          timeCalcTps = get_amount_data_needed(task) / bw +
                        SD_task_get_amount(task) / sg_host_speed(hosts[i]);
          cost = get_cost_data_prec_newVM(task, mhc);
        } else {
          // std::cout <<"vieil host\n";
          // timeCalcTps = ECT - getHC_occTime(mhc[hosts[i]]);
          timeCalcTps = ECT - sg_host_get_available_at(hosts[i]);
          cost = 0.;
        }
        cost += timeCalcTps * getHC_hC(mhc[hosts[i]]);
        std::cout << "budget :" << budgTsk << "\n";
        // calcul de l'indice de cost
        double idCost = calc_index_cost(budgTsk, Cbest, cost);
        // calcul de l'indice de temps
        double idTime = calc_index_cost(budgTsk, Cbest, cost);
        // calcul de l'heuristique
        double Theur = -1;
        if (idCost == 0) {
          Theur = 0;
        } else {
          Theur = idTime / idCost;
        }

        std::cout << "Theurbest = " << TheurBest << ", Theur = " << Theur
                  << "\n";
        // récup du best host
        if (Theur >= TheurBest) {

          std::cout << "Mieux, ancien host : " << sg_host_get_name(best_host)
                    << ", nouvel host : " << sg_host_get_name(hosts[i]) << "\n";
          // mise à jour du best host
          best_host = hosts[i];
          TheurBest = Theur;
        }

        selected_cost = cost;

        //--------------------------------------
        // remise à zéro des variables de vérif
        int iValVM;
        for (iValVM = 0; iValVM < 3; iValVM++) {
          if (valVM[iValVM] == 1) {
            valVM[iValVM] = 2;
          }
        }
        aTester = 0;
        //--------------------------------------

      } // fin for
      //    std::cout <<"cost : " <<selected_cost <<", autorisé :"
      //    <<get_subBudget_BoTlvl(BoTAct)<<"\n";

      if (selected_cost > get_subBudget_BoTlvl(BoTAct)) {
        best_host = NULL;
        selected_cost = 0;
      }
    }

    //====================================================
    // màj du budget, coup de balai et envoi
    //---------------------------------------

    //  std::cout <<"-----------gardé--------------- tâche :  "
    //  <<SD_task_get_name(task) <<", best host : EFT = " <<EFT <<" sur "
    //  <<sg_host_get_name(best_host) <<" ; coût : " <<costTmp <<", budget perso
    //  : " <<budgPTsk[task] << ", cagnotte restante : " <<cagnotteTmp <<"\n";

    //  cagnotteFin = cagnotteTmp;
    xbt_free(hosts);

  } // fin si schedulable
  else {
    std::cout << "pas schedulable\n";
    return NULL;
  }
  return best_host;
}

//========================================================================================
// outils sched spé arabnejad
//----------------------------
/**
 * Calcule l'earliest start time selon la méthode indiquée dans l'article
 *d'Arabnejad (pas de latence, coût de comm systématique -- on ne sait pas où se
 *déroule la tâche fille, pas trop le choix si on veut suivre la formule qu'ils
 *donnent)
 **/
double get_EST_arabnejad(SD_task_t task) {

  // si on est à t_{entry}
  if (strcmp(SD_task_get_name(task), "root") == 0) {
    return 0;
  } else {
    // sinon, l'EST dépend des tâches parentes
    double result = 0.;
    unsigned int i;
    double data_available = 0.;
    double redist_time = 0;
    double last_data_available;
    xbt_dynar_t parents = SD_task_get_parents(task);
    sg_host_t *hosts = sg_host_list();
    double bw = sg_host_route_bandwidth(
        hosts[2], hosts[3]); // mini abus : bw est la même partout

    // parcours de la liste des parents (pour trouver le data dispo le plus
    // tard)
    if (!xbt_dynar_is_empty(parents)) {

      SD_task_t parent;
      last_data_available = 0.0;

      xbt_dynar_foreach(parents, i, parent) {
        // normal case : the parent is a comm
        if (SD_task_get_kind(parent) == SD_TASK_COMM_E2E) {
          sg_host_t *parent_host = SD_task_get_workstation_list(parent);

          // Estimate the redistribution time from this parent
          // si la comm ne coûte rien, redist_time = 0
          if (SD_task_get_amount(parent) <= 1e-6) {
            redist_time = 0.;
          }
          // sinon, on a quelques calculs à faire
          else {
            redist_time = SD_task_get_amount(parent) / bw;
          } // fin de si la comm n'est pas de 0
          // les données des "tâches" de communication sont disponibles une fois
          // les données totalement envoyées
          data_available = SD_task_get_start_time(parent) + redist_time;
        } // fin du cas où la tâche parente est une communication
        // no transfer, control dependency -- cas où la tâche précédente est une
        // tâche exécutable
        if (SD_task_get_kind(parent) == SD_TASK_COMP_SEQ) {
          data_available = SD_task_get_finish_time(parent);
        } // fin du cas où la tâche parente est une tâche exécutable
        // mise à jour du moment le plus tardif pour lequel les données d'une
        // tâche seront disponibles
        if (last_data_available < data_available) {
          last_data_available = data_available;
        }
      } // fin de foreach parent
      xbt_dynar_free_container(&parents);
      return last_data_available;
    } // fin si parents pas vide
    else {
      // si pas de parent, dispo tout de suite (vu la tronche du dag, hautement
      // improbable, mais bon)
      return 0;
    } // fin de si pas de parent
  }   // fin de si on n'est pas à t_{entry}
}

/**
 * répartit le budget entre les niveaux en mode "All in" :
 * le niveau le plus prio commence avec tout le budget, les autres zéro
 **/
void repAllIn(std::map<int, BoTlvl> &mB, int lvlMax, double budget) {
  std::map<int, BoTlvl>::iterator it = mB.begin();
  for (it = mB.begin(); it != mB.end(); ++it) {
    if (get_lvl_BoTlvl(it->second) ==
        lvlMax) { // si on a le BoT qui a la plus haute prio, on lui donne tout
                  // le budget
      affBoT(it->second);
      std::cout << " budget accordé :" << budget << "\n";
      set_subBudget_BoTlvl(it->second, budget);
    } else {
      set_subBudget_BoTlvl(it->second, 0);
    }
  }
}

//========================================================================================
// gestion du BoTlvl
//-------------------
std::set<SD_task_t> get_set_BoTlvl(BoTlvl BoTl) { return BoTl.BoT; }

double get_subBudget_BoTlvl(BoTlvl BoTl) { return BoTl.subBudget; }

double get_lvl_BoTlvl(BoTlvl BoTl) { return BoTl.lvl; }

void add_task_BoTlvl(BoTlvl &BoTl, SD_task_t task) { BoTl.BoT.insert(task); }

void set_subBudget_BoTlvl(BoTlvl &BoTl, double newSubBudget) {
  BoTl.subBudget = newSubBudget;
}

void set_lvl_BoTlvl(BoTlvl &BoTl, double newLvl) { BoTl.lvl = newLvl; }

// ini : -1 car 0 est une valeur tout à fait possible
void init_BoTlvl(BoTlvl &BoTl) {
  BoTl.subBudget = -1;
  BoTl.lvl = -1;
}

void delete_task_BoTlvl(SD_task_t t, BoTlvl &BoTl) { BoTl.BoT.erase(t); }

void affBoT(BoTlvl BoTl) {
  std::cout << "lvl : " << get_lvl_BoTlvl(BoTl) << "\n";
  std::set<SD_task_t> sb = get_set_BoTlvl(BoTl);

  std::set<SD_task_t>::iterator it;
  for (it = sb.begin(); it != sb.end(); ++it) {
    std::cout << "\t " << SD_task_get_name(*it) << "\n";
  }
}

//========================================================================================
// calcul du niveau des tâches
//-----------------------------

// calcule le rang de la tâche demandée, celui de ses enfants, stocke le tout
// dans la map donnée, et renvoie le nombre de niveaux trouvés
int calc_rank_arabnejad(std::map<SD_task_t, int> &mTsklvl, SD_task_t task) {
  int maxLvl = 0;
  if (strcmp(SD_task_get_name(task), "end") == 0) {
    // tâche de fin, --> 1
    mTsklvl[task] = 1;
  } else {
    if (mTsklvl.find(task) == mTsklvl.end()) {
      // on va devoir checker le rang de chaque enfant de la tâche
      xbt_dynar_t children = SD_task_get_children(task);
      SD_task_t child;
      int iChild;
      int lvl;
      xbt_dynar_foreach(children, iChild, child) {
        if (mTsklvl.find(task) == mTsklvl.end()) {
          calc_rank_arabnejad(mTsklvl, child);
        } // fin si child n'a pas déjà son rang de calculé
        lvl = mTsklvl[child];
        if (lvl > maxLvl) { // mise à jour de la valeur max de lvl
          maxLvl = lvl;
        } // fin màj maxLvl

      } // fin du parcours des fils

      // calcul du niveau et ajout de la tâche dans la map
      if (SD_task_get_kind(task) == SD_TASK_COMM_E2E) {
        // si on a affaire à une comm, sa valeur est égale à celle de la tâche
        // de plus haut niveau qui l'a calculée
        mTsklvl[task] = maxLvl;
      } else {
        // sinon, on incrémente
        mTsklvl[task] = maxLvl + 1;
      }
      // test
      std::cout << SD_task_get_name(task) << ", de rang :" << maxLvl + 1
                << "\n";

    } // fin si task n'a pas déjà son rang d'existant
  }   // fin si pas end

  // std::cout <<"maxlvl : " <<maxLvl+1<<"\n";
  return maxLvl + 1;
}

/**
 * Remplit la map associant un niveau à un ensemble de tâches (et crée ledit
 *ensemble), retourne le nombre de niveaux créés Les budgets sont, par défaut,
 *mis à zéro
 *
 * mTmp : map temporaire pour un accès simple et rapide + check de si solution
 *déjà calculée pendant le calcul mLvlSet : map au cas où un article sorte pour
 *proposer une variante changeant le calcul des iveaux, mais un tableau aurait
 *pu faire l'affaire
 **/
int get_map_rank_arabnejad(std::map<int, BoTlvl> &mLvlSet,
                           std::map<SD_task_t, int> &mTmp, xbt_dynar_t dag) {
  // rappel : check si l'élément n'a pas été mis dans la map :
  // if(mTmp.find(t)== mTmp.end())

  // création de la map
  int nbLvl = calc_rank_arabnejad(mTmp, get_root(dag));

  // génération des BoT par niveau
  int iDag;
  SD_task_t task;
  xbt_dynar_foreach(dag, iDag, task) {
    // seules les tâches de calcul m'intéressent
    //    if (SD_task_get_kind(task) != SD_TASK_COMM_E2E) {

    if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {
      int lvl = mTmp[task];
      if (mLvlSet.find(lvl) == mLvlSet.end()) {
        // créer le BoT nécessaire, et ajouter la paire à la map
        BoTlvl newBoT;
        init_BoTlvl(newBoT);
        set_lvl_BoTlvl(newBoT, lvl);
        add_task_BoTlvl(newBoT, task);
        mLvlSet[lvl] = newBoT;
        // std::cout <<"\tnouveau BoT à créer : "<<lvl <<", pour la tâche "
        // <<SD_task_get_name(task)<<"\n";

      } else {
        // le BoT existe déjà, il faut ajouter la tâche au BoT
        //	std::cout << "BoT déjà existant, ancien BoT contient :\n";
        //      affBoT(mLvlSet[lvl]);

        add_task_BoTlvl(mLvlSet[lvl], task);

        //      std::cout <<"\npost ajout :\n";
        //      affBoT(mLvlSet[lvl]);
      }
    }
  }
  return nbLvl;
}

SD_task_t get_selected_task_arabnejad(BoTlvl BoT) {
  std::set<SD_task_t> BoTAct = get_set_BoTlvl(BoT);

  SD_task_t selected_task;
  double min_EST = std::numeric_limits<double>::max();
  double EST;
  std::set<SD_task_t>::iterator it;
  for (it = BoTAct.begin(); it != BoTAct.end(); ++it) {
    EST = get_EST_arabnejad(*it);
    // std::cout <<"Sélection de la tâche : " <<SD_task_get_name(*it) <<", EST :
    // " <<EST <<", min = "<<min_EST  <<"\n";

    if (EST <= min_EST) {
      min_EST = EST;
      selected_task = *it;
      // std::cout << "EST gardé ! " <<EST <<"\n";
    }

  } // fin de sélection de la tâche à placer
  return selected_task;
}

double calc_index_time(double ECTmax, double ECTmin, double ECTAct) {
  double res = 0.;
  if (ECTmax != ECTmin) {
    res = (ECTmax - ECTAct) / (ECTmax - ECTmin);
  }
  return res;
}

double calc_index_cost(double subBudget, double Cbest, double CAct) {
  double res = 0.;
  if ((subBudget > CAct) && (subBudget > Cbest)) {
    res = (subBudget - CAct) / (subBudget - Cbest);
  }
  return res;
}

//========================================================================================
/**
 * Fait l'ordonnancement et retourne le mapping tâche -> hôte + l'odre
 *d'attribution (vu qu'il remet sans cesse en question l'ordre d'attribution et
 *que seul
 *
 * ordo_t = tableau des tâches ordonnancées (données dans l'ordre de leur
 *ordonnancement) wf = workflow à placer m = mapping <tâche, hôte> effectué par
 *l'algorithme
 **/
void algo_offline_arabnejad(Workflow &wf, SD_task_t ordoT[],
                            std::map<SD_task_t, sg_host_t> &m, double deadline,
                            double budget, double cagnotte,
                            std::map<sg_host_t, HostCost> mhc) {
  xbt_dynar_t dag = getDagWf(wf);
  size_t cursor;
  SD_task_t task;

  //-----------------------------------------------------------
  // pre-calculi: repartition of various budgets, for example
  //.........................................................

  size_t total_nhosts = sg_host_count();
  sg_host_t *hosts = sg_host_list();

  //========== test rank ===========
  std::map<SD_task_t, int> mRankA;
  std::map<int, BoTlvl> mLvlSet;
  std::cout << "test d'arabnejad rank\n";
  int nbLvl = get_map_rank_arabnejad(mLvlSet, mRankA, dag);

  // création de la liste des tâches triées par rang décroissant
  // int nbLvl = getTasksSortedByRank(lT, dag, speed_m, bw ,lat_m);
  //===============================================

  // répartition du budget :
  // All in : on donne tout au niveau le plus prio, 0 aux autres :
  repAllIn(mLvlSet, nbLvl, budget);

  double timeCalc = 0;
  double cagnotteFin = 0;

  //===========================================================
  // let's fork
  int fd[2];
  pid_t childpid;

  if (pipe(fd) == -1) {
    fprintf(stderr, "pipe: %s", strerror(errno));
    exit(EXIT_FAILURE);
  }
  if ((childpid = fork()) == -1) {
    perror("fork");
    exit(1);
  }
  if (childpid == 0) {
    close(fd[0]); /* close read */
    unsigned int cursor;
    SD_task_t task;

    std::map<SD_task_t, size_t>
        nb_ord; // map associant une tâche au numéro indiquant dans quel ordre
                // elles ont été ordonnancées
    size_t nb_ordo_tmp =
        0; // compteur indiquant à quel moment la tâche a été ordonnancée

    //==================================================
    xbt_dynar_foreach(dag, cursor, task) { SD_task_watch(task, SD_DONE); }
    //==================================================
    // scheduling
    //------------

    xbt_dynar_t changed_tasks = xbt_dynar_new(sizeof(SD_task_t), NULL);

    // attribution des VMs : parcours des BoT par niveau
    for (int i = nbLvl; i > 0; i--) {
      // récupération du BoT
      //      BoTlvl BoTAct = mLvlSet[i];
      SD_task_t selected_task;
      sg_host_t selected_host;
      // Sélection de la tâche à placer
      //      selected_task = get_selected_task_arabnejad(BoTAct);

      std::cout << "\n-------\nBoT n°" << i << ", contenant : \n";
      affBoT(mLvlSet[i]);
      std::cout << "budget : " << get_subBudget_BoTlvl(mLvlSet[i]) << "\n";
      std::cout << "\n--------\n";
      do {
        selected_task = get_selected_task_arabnejad(mLvlSet[i]);
        // choix de l'hôte compte tenu du budget :
        // si la tâche est plaçable, (ready) la placer (puisqu'on peut être dans
        // le cas où ce n'est pas le cas)
        std::cout << "tâche sélectionnée : " << SD_task_get_name(selected_task)
                  << "\n";
        // if(SD_task_get_state(selected_task) == SD_SCHEDULABLE){

        std::cout << "schedulable !\n";
        double costTmp = 0;
        // get_best_host
        selected_host = SD_task_get_best_host_arabnejad(
            selected_task, mLvlSet[i], mhc, mLvlSet, costTmp);
        // std::cout <<"host : " <<sg_host_get_name(selected_host)<<"\n";
        std::cout << "tâche " << SD_task_get_name(selected_task)
                  << ", hôte : " << selected_host << "\n";
        if (selected_host == NULL) { // s'il n'existe pas d'host respectant le
                                     // budget virer la tâche
          std::cout << "tâche à host null, reste "
                    << get_subBudget_BoTlvl(mLvlSet[i]);
          ;
          delete_task_BoTlvl(selected_task, mLvlSet[i]);
          // transmettre le budget
        }
        // sinon, on la place et on retranche son coût du budget
        else {
          double nouvBudg = get_subBudget_BoTlvl(mLvlSet[i]) - costTmp;
          set_subBudget_BoTlvl(mLvlSet[i], nouvBudg);

          // attribution
          SD_task_schedulel(selected_task, 1, selected_host);
          /*
          // attribution des enfants (car on ne suit pas l'ordre d'exécution
          pour affecter les tâches, les attributions de comm automatiques ne se
          feront pas d'elles-mêmes. On place donc ce qu'on sait : on aura au
          moins de la comm sur l'hôte choisi, vu que la tâche va envoyer ses
          résultats) xbt_dynar_t children = SD_task_get_children(selected_task);
          if (!xbt_dynar_is_empty(children)) {
            SD_task_t child;
            unsigned int i;
            xbt_dynar_foreach(children, i, child) {
              if (SD_task_get_kind(child) == SD_TASK_COMM_E2E) {
                if(SD_task_get_state(child) == SD_NOT_SCHEDULED){
                  std::cout <<"child : " <<SD_task_get_name(child)<<"\n";
                  SD_task_schedulel(child, 1, selected_host);
                }
              }
            }
          }
          */

          m[selected_task] = selected_host;
          // associer son numéro d'ordre d'ordonnancement
          nb_ord[selected_task] =
              nb_ordo_tmp; // vraiment pour garder le template TODO: faire la
                           // même proprement
          // incrémenter pour avoir le numéro suivant
          nb_ordo_tmp++;
          double min_finish_time = finish_on_at(selected_task, selected_host);
          //=======================
          // SimDag allows tasks to be executed concurrently when they can by
          // default. Yet schedulers take decisions assuming that tasks wait for
          // resource availability to start. The solution (well crude hack is to
          // keep track of the last task scheduled on a host and add a special
          // type of dependency if needed to force the sequential execution
          // meant by the scheduler. If the last schxeduled task is already
          // done, has failed or is a predecessor of the current task, no need
          // for a new dependency

          SD_task_t last_scheduled_task =
              sg_host_get_last_scheduled_task(selected_host);

          // pour la prise en compte du nombre de coeurs, c'est là qu'il faut
          // taper (rajouter un compteur ou un tableau, ou alors traduire le
          // nombre de coeurs dans la plateforme (un gros cluster de machine
          // mono-coeur
          if (last_scheduled_task &&
              (SD_task_get_state(last_scheduled_task) != SD_DONE) &&
              (SD_task_get_state(last_scheduled_task) != SD_FAILED) &&
              !SD_task_dependency_exists(
                  sg_host_get_last_scheduled_task(selected_host),
                  selected_task)) {
            SD_task_dependency_add("resource", NULL, last_scheduled_task,
                                   selected_task);
          }
          sg_host_set_last_scheduled_task(selected_host, selected_task);
          sg_host_set_available_at(selected_host, min_finish_time);

          //=======================

          std::cout << "tâche " << SD_task_get_name(selected_task)
                    << ", hôte : " << sg_host_get_name(selected_host) << "\n";

          // simulate with update
          SD_simulate_with_update(-1.0, changed_tasks);

          // la retirer du BoT
          delete_task_BoTlvl(selected_task, mLvlSet[i]);
        }

        //}
        /*
        else{
        // la virer du BoT, elle ne sera jamais possible
          std::cout <<"tâche " <<SD_task_get_name(selected_task) <<", pas
        schedulable\n"; delete_task_BoTlvl(selected_task, mLvlSet[i]);

        }
        */
        //	std::cout<<"taille : " <<get_set_BoTlvl((BoTAct)).size() << " et
        // budget : " <<get_subBudget_BoTlvl(BoTAct) <<"\n";
        // delete_task_BoTlvl(selected_task, BoTAct);

      } while ((get_set_BoTlvl(mLvlSet[i]).size() != 0) &&
               (get_subBudget_BoTlvl(mLvlSet[i]) >
                0)); // fin du placement de la tâche :
      // soit le BoT est vide
      // soit on a épuisé le budget
      // dans les deux cas, on prend le budget restant et on l'ajoute au niveau
      // suivant
      if (i > 1) {
        double nouvBudgFils = get_subBudget_BoTlvl(mLvlSet[i - 1]) +
                              get_subBudget_BoTlvl(mLvlSet[i]);
        std::cout << "budget niveau suivant : "
                  << get_subBudget_BoTlvl(mLvlSet[i - 1])
                  << ", budget niveau actuel : "
                  << get_subBudget_BoTlvl(mLvlSet[i])
                  << "reste : " << nouvBudgFils << "\n";
        set_subBudget_BoTlvl(mLvlSet[i - 1], nouvBudgFils);
      }

    } // fin parcours des niveaux

    std::cout << "------------------- sched fait -------------------\n";
    //===================================================================
    const char nomAlgo[] = "arabnejad";
    credoFile(nomAlgo, budget, cagnotte);
    //===================================================================
    std::cout << "------------------- credo fait -------------------\n";

    //==================================================
    // sending results
    //---------------------
    /*
       Wf is the same for father and son.
       Same order of tasks given by following foreach.
       For each task, send
       an u int for ordo
       an u int for size of hostname
       char[] for hostname
    */

    xbt_dynar_foreach(getDagWf(wf), cursor, task) {
      if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {
        // gestion des tâches non placées
        if (nb_ord.find(task) == nb_ord.end()) {
          nb_ord[task] = nb_ordo_tmp; // vraiment pour garder le template TODO:
                                      // faire la même proprement
          // incrémenter pour avoir le numéro suivant
          nb_ordo_tmp++;
        }

        const char *nom;
        if (m.find(task) != m.end()) {
          nom = sg_host_get_name(m[task]);
        } else {
          nom = "null";
        }

        size_t ordo_number;
        //	if(nb_ord.find(task) != nb_ord.end()){
        std::cout << "envoi de " << SD_task_get_name(task) << "\n";
        ordo_number = nb_ord[task];
        if (write(fd[1], &ordo_number, sizeof(size_t)) <= 0) {
          printf("Error write ordo\n");
          exit(-1);
        }

        size_t hostnamelen = strlen(nom);
        //	unsigned int hostnamelen = strlen(sg_host_get_name(m[task])) ;
        if (write(fd[1], &hostnamelen, sizeof(size_t)) <= 0) {
          printf("Error write hostname length\n");
          exit(-1);
        }

        while (hostnamelen != 0) {
          size_t nb_bytes = write(fd[1], nom, hostnamelen);
          // size_t nb_bytes=write(fd[1], sg_host_get_name(m[task]),
          // hostnamelen) ;
          if (nb_bytes < 0) {
            char output[100];
            sprintf(output, "problème de write dans pipe\n");
            perror(output);
            exit(1);
          }
          hostnamelen -= nb_bytes;
        }
        //}
      }
    }

    //==============================================
    // cleaning...
    // tachesSrc.clear();
    //  mapInfosTask.clear();
    freeUtilsMath(); // freeing the generator
    //  xbt_dynar_free_container(&dax);
    deleteWf(wf);

    size_t total_nhosts = sg_host_count();
    sg_host_t *hosts = sg_host_list();

    for (cursor = 0; cursor < total_nhosts; cursor++) {
      free(sg_host_user(hosts[cursor]));
      sg_host_user_set(hosts[cursor], NULL);
    }

    xbt_free(hosts);
    SD_exit();
    exit(0);

  } // end child
  else {
    // fait l'ordo
    close(fd[1]); /* close write */
    /*
       Wf is the same for father and son.
       Same order of tasks given by following foreach.
       For each task, send
       an u int for ordo
       an u int for size of hostname
       char[] for hostname
    */

    xbt_dynar_foreach(getDagWf(wf), cursor, task) {
      if (SD_task_get_kind(task) == SD_TASK_COMP_SEQ) {

        size_t ordo_number = 0;
        if (read(fd[0], &ordo_number, sizeof(size_t)) <= 0) {
          printf("Error read ordo\n");
          exit(-1);
        }

        size_t hostnamelen = strlen(SD_task_get_name(task));
        if (read(fd[0], &hostnamelen, sizeof(size_t)) <= 0) {
          printf("Error read hostname length\n");
          exit(-1);
        }

        unsigned int hostLen =
            hostnamelen; // to keep the lenght of the recieved host name
                         // somewhere and be able to put the \0 at the end of
                         // the reception
        char *nomHost = (char *)malloc(sizeof(char) * (hostnamelen + 1));
        while (hostnamelen != 0) {
          size_t nb_bytes = read(fd[0], nomHost, hostnamelen);
          if (nb_bytes < 0) {
            char output[100];
            sprintf(output, "problème de read dans pipe\n");
            perror(output);
            exit(1);
          }
          hostnamelen -= nb_bytes;
        } // end of receive name of host

        nomHost[hostLen] = '\0';
        std::cout << "j'ai lu le nom de l'hôte : " << nomHost
                  << " pour la tâche : " << SD_task_get_name(task)
                  << " d'indice : " << ordo_number << "\n";
        if (strcmp(nomHost, "null") != 0) {
          m[task] = sg_host_by_name(nomHost);
          ordoT[ordo_number] = task;
        }

        free(nomHost);
      } // end of if task is executable
    }   // end of foreach

    int stat_val;
    waitpid(childpid, &stat_val, 0);
  } // end parent
} // end of func