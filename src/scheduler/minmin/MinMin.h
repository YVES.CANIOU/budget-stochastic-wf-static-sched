#ifndef MINMIN_H
#define MINMIN_H

#include "workflow.h"

sg_host_t SD_task_get_best_host_minmin(SD_task_t task);

void algo_offline_minmin(Workflow &wf, SD_task_t ordoT[],
                         std::map<SD_task_t, sg_host_t> &m);
#endif