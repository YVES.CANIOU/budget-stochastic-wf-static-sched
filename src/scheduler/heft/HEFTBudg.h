#ifndef HEFT_BUDG_H
#define HEFT_BUDG_H

// explosion des limites sup des doubles --> voir si gmp n'aurait pas un type de
// remplacement
#include "workflow.h"
#include "hostCost.h"

sg_host_t SD_task_get_best_host_heft_budget(
    SD_task_t task, std::map<sg_host_t, HostCost> mhc,
    std::map<SD_task_t, double> budgPTsk, double cagnotte, double &timeCalc,
    double &cagnotteFin);

void algo_offline_heft_budget(Workflow &wf, SD_task_t ordoT[],
                              std::map<SD_task_t, sg_host_t> &m,
                              double deadline, double budget, double cagnotte,
                              std::map<sg_host_t, HostCost> mhc);
#endif