#include <cmath>
#include <iostream>
#include <map>
#include <stdio.h>
#include <string.h>

#include "dividePot.h"
#include "harvestingData.h"
#include "hostCost.h"
#include "toolsSched.h"
// algos
#include "HEFT.h"
#include "algo_CGFair_sec_rep.h"
#include "algo_arabnejad.h"
#include "algo_critical_greedy.h"
#include "algo_critical_greedy_extended_total_fair.h"

#include "HEFTBudg.h"
#include "HEFTBudgMult.h"

#include "HEFTSecRep.h"
#include "HEFTSecRepInv.h"

#include "MinMin.h"
#include "MinMinBudg.h"
#include "MinMinBudgDiscrEx.h"

#include "FTL.h"
#include "FTLMultFirstTask.h"
#include "FTLMultLastTaskLvl.h"
#include "HEFTBudgCascade.h"
#include "HEFTBudgMult2EveryTask.h"
#include "HEFTBudgMult2FirstEveryTask.h"
#include "HEFTBudgMult2FirstTask.h"
#include "HEFTBudgMult2Path.h"
#include "HEFTBudgPath.h"

#define MAXLEN_SERVERNAME 30

int main(int argc, char **argv) {
  size_t cursor;

  SD_init(&argc, argv);

  // Initialising the generator of random values
  // To initialise the generator using a known seed :
  // iniUtilsMath(yourDoubleValue);
  double seed = std::stod(argv[2]);
  iniUtilsMath(seed);

  //=================================
  // Loading the data for the DAG
  //------------------------------
  Workflow wf;
  loadDaxWf(wf, argv[3]);

  int bidule;
  SD_task_t biduleTask;

  /*
    std::cout<<"test : contenu du wf dans le main : \n";
    xbt_dynar_foreach(wf.dag, bidule, biduleTask){
          if (SD_task_get_kind(biduleTask) != SD_TASK_COMM_E2E){
              std::cout <<"    tâche : " <<SD_task_get_name(biduleTask) <<",
    mean : " <<SD_task_get_amount(biduleTask) <<"\n" ;
          }
    }
  */

  //====================================
  // Loading the platform
  //--------------------------
  SD_create_environment(argv[4]);

  //===============================================
  //  Allocating the host attribute :
  // each host is associated to informations (here : last scheduled task
  // and finish time)
  size_t total_nhosts = sg_host_count();
  sg_host_t *hosts = sg_host_list();

  for (cursor = 0; cursor < total_nhosts; cursor++) {
    sg_host_user_set(hosts[cursor], xbt_new0(struct _HostAttribute, 1));
  }

  //===============================================
  // costs and time infos for each host
  //-----------------------------------
  std::map<sg_host_t, HostCost> mhc;
  // générer les valeurs de coût pour les hosts
  double ratio = std::stod(argv[6]);
  for (cursor = 0; cursor < total_nhosts; cursor++) {
    HostCost nouv;
    barbSetHostCost(nouv, hosts[cursor], ratio);
    // print costs
    // const char * host_name = sg_host_get_name(hosts[cursor]);
    // std::cout <<"host : " << host_name <<", cost per time unit : "
    // <<getHC_hC(nouv) <<", ini cost : " <<getHC_iniC(nouv) <<std::endl;
    mhc[hosts[cursor]] = nouv;
  }

  //=================================
  // Data of the problem
  //---------------------
  double pCSD = -1.; // percent sd : sd = mean*pCSD
  pCSD = std::stod(argv[5]);

  // xbt_dynar_foreach(wf.dag, bidule, biduleTask) {
  //   std::cout << "___________________ voici la quantité de travaille de "
  //             << SD_task_get_name(biduleTask)
  //             << ", mean : " << SD_task_get_amount(biduleTask) << std::endl;
  // }

  // generating the times of the executable tasks (the size of created files is
  // not supposed to be random)
  double sdTh = -1.;
  sdTh = std::stod(argv[9]);
  randAmountWTaskWf(wf, sdTh);
  // generate the "real" values for the amount of work of the tasks
  generateInfosTasksWf(wf, pCSD);
  double deadline = std::stod(argv[7]);
  double budget = std::stod(argv[8]);
  double budget2 = std::stod(argv[8]);
  double cagnotte = 0.;

  // print the generated times for each task
  FILE *realTimes;
  char realTimesFich[] = "realTimes";
  realTimes = fopen(realTimesFich, "a+");
  if (realTimes != NULL) {

    std::cout << "test : real amount : \n";
    double flopsToSec = 4.2 * pow(10, 9);
    xbt_dynar_foreach(wf.dag, bidule, biduleTask) {
      if (SD_task_get_kind(biduleTask) != SD_TASK_COMM_E2E) {
        std::cout << "    tâche : " << SD_task_get_name(biduleTask)
                  << ", mean : "
                  << getRealWorkIst(getInfoStochTaskWf(wf, biduleTask)) /
                         flopsToSec
                  << "\n";
        fprintf(realTimes, "%s %f\n", SD_task_get_name(biduleTask),
                getRealWorkIst(getInfoStochTaskWf(wf, biduleTask)) /
                    flopsToSec);
      }
    }
  }

  fclose(realTimes);

  //==================================================
  // structures to generate and stock the scheduling
  //------------------------------------------------
  size_t nbTasks =
      get_nb_calc_tasks(getDagWf(wf)); // number of tasks in the workflow
  SD_task_t ordoTasks[nbTasks];

  double res[2];
  double resExpensive[2];

  //===============================================================================
  // to keep which communications are sent by root and where to count them
  //-----------------------------------------------------------------------
  std::map<SD_task_t, SD_task_t>
      mCommRootDest; // map <communication from root, dest task>
  SD_task_t task;
  size_t cursorComm;
  xbt_dynar_foreach(getDagWf(wf), cursorComm, task) {
    if (strcmp(SD_task_get_name(task), "root") == 0) {
      xbt_dynar_t startChildren = SD_task_get_children(task);
      SD_task_t sChild, sGChild;
      size_t cursorChild, cursorGC;
      xbt_dynar_foreach(startChildren, cursorChild, sChild) {
        // std::cout <<"     enfant de "<<SD_task_get_name(task) <<" : "
        // <<SD_task_get_name(sChild) <<"\n";
        xbt_dynar_t startGC = SD_task_get_children(sChild);

        xbt_dynar_foreach(startGC, cursorGC, sGChild) {
          // std::cout <<"             petit enfant de "<<SD_task_get_name(task)
          // <<" et enfant de "<<SD_task_get_name(sChild) <<" : "
          // <<SD_task_get_name(sGChild) <<"\n";

          mCommRootDest[sChild] = sGChild;

          xbt_dynar_free_container(&startGC);
        }
      }
      xbt_dynar_free_container(&startChildren);
    }
  }
  //===============================================================================

  minCostSched(wf, mhc, res);
  maxCostSched(wf, mhc, resExpensive, mCommRootDest);

  // pire temps :
  double wTime = res[1];
  // réattribution du budget :
  // pour chaque mois, payer quantité d'infos * coût de stockage par Go
  int nbMonths = 1 + wTime / (3600 * 24 * 30);
  //======================================================
  // calcul de la quantité de données à transférer :
  double amountData = 0;

  SD_task_t root, end;
  // récupération de root et de end
  xbt_dynar_foreach(getDagWf(wf), cursorComm, task) {
    if (strcmp(SD_task_get_name(task), "root") == 0) {
      xbt_dynar_t startChildren = SD_task_get_children(task);
      size_t cursorChild;
      SD_task_t sChild;
      xbt_dynar_foreach(startChildren, cursorChild, sChild) {
        // std::cout <<"     enfant de "<<SD_task_get_name(task) <<" : "
        // <<SD_task_get_name(sChild) <<"\n";
        amountData += SD_task_get_amount(sChild);
      }
      xbt_dynar_free_container(&startChildren);
    }

    //--------------------------------------------------------
    if (strcmp(SD_task_get_name(task), "end") == 0) {
      xbt_dynar_t endParents = SD_task_get_parents(task);
      size_t cursorPar;
      SD_task_t ePar;
      xbt_dynar_foreach(endParents, cursorPar, ePar) {
        // std::cout <<"     enfant de "<<SD_task_get_name(task) <<" : "
        // <<SD_task_get_name(sChild) <<"\n";
        amountData += SD_task_get_amount(ePar);
      }
      xbt_dynar_free_container(&endParents);
    }
  }

  //======================================================

  budget = budget - (nbMonths * amountData / 1000000000 * STORE_COST +
                     amountData / 1000000000 * TRANSF_COST);
  // budget = budget - nbMonths * amountData * getHC_storeC(mhc[hosts[0]]);
  std::cout << " vrai budget tout neuf : " << budget
            << ", nb mois : " << nbMonths
            << ", quantité de données :" << amountData
            << ", coût de stockage : " << getHC_storeC(mhc[hosts[0]]) << "\n";

  //===========================
  // offline algorithm
  //-------------------
  std::map<SD_task_t, sg_host_t> m;
  if (strcmp(argv[1], "rien") == 0) {
    printf("algo rien\n");
  }
  /******************************* ALGOS MINMIN ******************************/
  else if (strcmp(argv[1], "minmin") == 0) {
    printf("algo minmin\n");
    algo_offline_minmin(wf, ordoTasks, m);
  } else if (strcmp(argv[1], "minminBudg") == 0) {
    printf("algo minmin à budget\n");
    algo_offline_minmin_budget(wf, ordoTasks, m, deadline, budget, cagnotte,
                               mhc);
  } else if (strcmp(argv[1], "minminBudgDiscrEx") == 0) {
    printf("algo minmin à budget avec discrimination en cas d'ex aequo\n");
    algo_offline_minmin_budget_discr_ex(wf, ordoTasks, m, deadline, budget,
                                        cagnotte, mhc);
  }

  /****************************** ALGOS HEFT *********************************/
  else if (strcmp(argv[1], "heft") == 0) {
    printf("algo heft\n");
    algo_offline_heft(wf, ordoTasks, m);
  } else if (strcmp(argv[1], "heftBudg") == 0) {
    printf("algo heft à budget\n");
    algo_offline_heft_budget(wf, ordoTasks, m, deadline, budget, cagnotte, mhc);
  } else if (strcmp(argv[1], "heftBudgMult") == 0) {
    printf("algo heftBudgMult (un heft budg puis un heft budg)\n");
    algo_offline_heft_budgMult(wf, ordoTasks, m, deadline, budget, cagnotte,
                               mhc);
  } else if (strcmp(argv[1], "heftBudgMult2FirstTask") == 0) {
    printf("algo heftBudgMult (un heft budg puis un heft budg) avec une "
           "répartition du lefover sur la première tâche\n");
    algo_offline_heft_budg_mult2_first_task(wf, ordoTasks, m, deadline, budget,
                                            cagnotte, mhc);
  } else if (strcmp(argv[1], "heftBudgMult2EveryTask") == 0) {
    printf("algo heftBudgMult (un heft budg puis un heft budg) avec une "
           "répartition du lefover sur toutes les tâches\n");
    algo_offline_heft_budg_mult2_every_task(wf, ordoTasks, m, deadline, budget,
                                            cagnotte, mhc);
  } else if (strcmp(argv[1], "heftBudgMult2FirstEveryTask") == 0) {
    printf("algo heftBudgMult (un heft budg puis un heft budg) avec une "
           "répartition du lefover sur toutes les tâches et sur la première "
           "tâche\n");
    algo_offline_heft_budg_mult2_first_every_task(wf, ordoTasks, m, deadline,
                                                  budget, cagnotte, mhc);
  } else if (strcmp(argv[1], "heftBudgPath") == 0) {
    printf("algo heftBudg avec une répartition du budget en fonction des "
           "chemins\n");
    algo_offline_heft_budg_path(wf, ordoTasks, m, deadline, budget, cagnotte,
                                mhc);
  } else if (strcmp(argv[1], "heftBudgMult2Path") == 0) {
    printf("algo heftBudgMult (un heft budg puis un heft budg) avec une "
           "répartition du leftover en fonction des chemins\n");
    algo_offline_heft_budg_mult2_path(wf, ordoTasks, m, deadline, budget,
                                      cagnotte, mhc);
  } else if (strcmp(argv[1], "heftBudgCascade") == 0) {
    printf("algo heftBudg répartition de la cagnote non pas à la tâche "
           "suivante mais sur les fils de la tâche\n");
    algo_offline_heft_budg_cascade(wf, ordoTasks, m, deadline, budget, cagnotte,
                                   mhc);
  }

  /******************************* ALGOS FTL *********************************/
  else if (strcmp(argv[1], "ftl") == 0) {
    printf("algo avec une répartition du budget en fonction des niveaux\n");
    std::map<SD_task_t, double> newBudgPTsk;
    std::map<SD_task_t, unsigned int> eftPTsk;
    algo_offline_ftl(wf, ordoTasks, m, budget, cagnotte, mhc, eftPTsk,
                     newBudgPTsk);
  } else if (strcmp(argv[1], "ftlMultCascade") == 0) {
    printf("algo en fonction des niveaux avec une seconde repartition du "
           "leftover sur la première tâche\n");
    algo_offline_ftl_mult_first_task(wf, ordoTasks, m, deadline, budget,
                                     cagnotte, mhc, &notDivPot);
  } else if (strcmp(argv[1], "ftlMultCascadeNbChild") == 0) {
    printf("algo en fonction des niveaux avec une seconde repartition du "
           "leftover sur la première tâche et une répartition de la cagnotte "
           "en fonction du nombre de tâches fille\n");
    algo_offline_ftl_mult_first_task(wf, ordoTasks, m, deadline, budget,
                                     cagnotte, mhc, &divPotNBChildren);
  } else if (strcmp(argv[1], "ftlMultCascadeChildPath") == 0) {
    printf("algo en fonction des niveaux avec une seconde repartition du "
           "leftover sur la première tâche et une répartition de la cagnotte "
           "en fonction du poids de tout les chemins descendant de la tâche\n");
    algo_offline_ftl_mult_first_task(wf, ordoTasks, m, deadline, budget,
                                     cagnotte, mhc, &divPotWeighChildPath);
  } else if (strcmp(argv[1], "ftlMultCascadeChildDesc") == 0) {
    printf("algo en fonction des niveaux avec une seconde repartition du "
           "leftover sur la première tâche et une répartition de la cagnotte "
           "en fonction du poids de chaque tâche descendante de chaque tâche "
           "fille\n");
    algo_offline_ftl_mult_first_task(wf, ordoTasks, m, deadline, budget,
                                     cagnotte, mhc, &divPotDescendantChild);
  } else if (strcmp(argv[1], "ftlMultLastTaskLvl") == 0) {
    printf("algo avec une double répartition du budget en fonction des niveaux "
           "et des tâche retardant leur niveau\n");
    algo_offline_ftl_mult_last_task_lvl(wf, ordoTasks, m, deadline, budget,
                                        cagnotte, mhc);
  }

  /******************************* OTHERS ALGOS ******************************/
  else if (strcmp(argv[1], "heftSecRep") == 0) {
    printf("algo offline heft à seconde répartition\n");
    algo_offline_heft_sec_rep(wf, ordoTasks, m, deadline, budget, cagnotte,
                              mhc);
  } else if (strcmp(argv[1], "heftSecRepInv") == 0) {
    printf("algo offline heft à seconde répartition, en commençant par les "
           "premières tâches\n");
    algo_offline_heft_sec_rep_inv(wf, ordoTasks, m, deadline, budget, cagnotte,
                                  mhc);
  } else if (strcmp(argv[1], "arabnejad") == 0) {
    printf("algo d'arabnejad, dans sa meilleure repartition du budget (on "
           "donne tout au premier niveau)\n");
    algo_offline_arabnejad(wf, ordoTasks, m, deadline, budget, cagnotte, mhc);
  } else if (strcmp(argv[1], "criticalGreedy") == 0) {
    algo_offline_heft_critical_greedy(wf, ordoTasks, m, deadline, budget,
                                      cagnotte, mhc);
  } else if (strcmp(argv[1], "CGFair") == 0) {
    algo_offline_CG_total_fair(wf, ordoTasks, m, deadline, budget, cagnotte,
                               mhc, res[0], resExpensive[0]);
  } else if (strcmp(argv[1], "CGFairSecRep") == 0) {
    Workflow copyWf;
    loadDaxWf(copyWf, argv[3]);

    //!!!! seul le dag est chargé à l'identique ! ne surtout pas l'utiliser pour
    //! autre chose !
    // TODO un jour: faire une copie réelle du wf
    algo_offline_cgfair_sec_rep(wf, copyWf, ordoTasks, m, deadline, budget,
                                cagnotte, mhc, res[0], resExpensive[0]);
  }

  /***************************** DEFAULT CASE ********************************/
  else {
    printf("Sorry, the algorithm you want to call doesn't exist ! \n");
    freeUtilsMath(); // freeing the generator
    //  xbt_dynar_free_container(&dax);
    deleteWf(wf);

    for (cursor = 0; cursor < total_nhosts; cursor++) {
      free(sg_host_user(hosts[cursor]));
      sg_host_user_set(hosts[cursor], NULL);
    }

    xbt_free(hosts);
    SD_exit();
    exit(-1);
  }

  // patch : cas exceptionnel où il est possible d'arriver à end sans avoir
  // exécuté toutes les tâches (cybershake...)
  SD_task_t tacheFin;

  //==========================================================================================
  // allocation of the tasks to their respective host in the order given by the
  // algorithm
  //--------------------------------------------------------------------------------------
  double iniComp; // equivalent in number of instructions of the time needed to
                  // start the VM
  double newAmount; // new amount of work for the task (start time included)

  /**===================================================
   *	Assignment file generation
   *---------------------------------------------------*/

  FILE *assignmentsFile;
  assignmentsFile = fopen("static_sched", "a+");
  if (assignmentsFile != NULL) {
    for (size_t i = 0; i < nbTasks; i++) {
      std::cout << "      ajouté au fichier : tâche : "
                << SD_task_get_name(ordoTasks[i]) << " d'indice : " << i
                << "  sur l'hôte : " << sg_host_get_name(m[ordoTasks[i]])
                << "\n";
      fprintf(assignmentsFile, "%s %s\n", SD_task_get_name(ordoTasks[i]),
              sg_host_get_name(m[ordoTasks[i]]));
    }
  }
  fclose(assignmentsFile);
  /**===================================================*/

  for (size_t i = 0; i < nbTasks; i++) {
    std::cout << "      tâche : " << SD_task_get_name(ordoTasks[i])
              << " d'indice : " << i
              << "  sur l'hôte : " << sg_host_get_name(m[ordoTasks[i]]) << "\n";

    // If the scheduled task start a new VM, add the start time
    if (sg_host_get_last_scheduled_task(m[ordoTasks[i]]) == NULL) {
      iniComp =
          getHC_tmpIni(mhc[m[ordoTasks[i]]]) * sg_host_speed(m[ordoTasks[i]]);
      newAmount = SD_task_get_amount(ordoTasks[i]) + iniComp;
      SD_task_set_amount(ordoTasks[i], newAmount);
    }

    //=============================================================
    // si on ne commence pas une nouvelle vm, lier la dernière tâche de la VM à
    // la nouvelle tâche qu'on lui assigne
    else {
      // mini-patch: keeps the order given by the scheduling algorithm ?
      //  TODO: checker les minminbudg : apparemment
      if ((!SD_task_dependency_exists(
              sg_host_get_last_scheduled_task(m[ordoTasks[i]]),
              ordoTasks[i])) &&
          (strcmp("end", SD_task_get_name(ordoTasks[i])) != 0)) {
        SD_task_dependency_add("orderConservation", NULL,
                               sg_host_get_last_scheduled_task(m[ordoTasks[i]]),
                               ordoTasks[i]);
      }
    }
    //=============================================================

    /*
        // repérage de la tâche end
    if(strcmp("end", SD_task_get_name(ordoTasks[i])) == 0){
      tacheFin = ordoTasks[i];
    }
    */
    if (strcmp("end", SD_task_get_name(ordoTasks[i])) != 0) {
      SD_task_schedulel(ordoTasks[i], 1, m[ordoTasks[i]]);
      sg_host_set_last_scheduled_task(m[ordoTasks[i]], ordoTasks[i]);
    } else {
      tacheFin = ordoTasks[i];
    }
    /*
    // mini-patch: double communication time // n'est plus nécessaire : utiliser
    le xml adequat if (SD_task_get_kind(ordoTasks[i]) == SD_TASK_COMM_E2E) {
      SD_task_set_amount(ordoTasks[i], SD_task_get_amount(ordoTasks[i]) * 2);
    }
    */
  }

  // quand tout le reste est placé, placer end et la lier

  // If the scheduled task start a new VM, add the start time
  if (sg_host_get_last_scheduled_task(m[tacheFin]) == NULL) {
    iniComp = getHC_tmpIni(mhc[m[tacheFin]]) * sg_host_speed(m[tacheFin]);
    newAmount = SD_task_get_amount(tacheFin) + iniComp;
    SD_task_set_amount(tacheFin, newAmount);
  }

  //=============================================================
  // si on ne commence pas une nouvelle vm, lier la dernière tâche de la VM à la
  // nouvelle tâche qu'on lui assigne
  else {
    // mini-patch: keeps the order given by the scheduling algorithm ?

    if (!SD_task_dependency_exists(sg_host_get_last_scheduled_task(m[tacheFin]),
                                   tacheFin)) {
      SD_task_dependency_add("orderConservation", NULL,
                             sg_host_get_last_scheduled_task(m[tacheFin]),
                             tacheFin);
    }
  }

  SD_task_schedulel(tacheFin, 1, m[tacheFin]);
  sg_host_set_last_scheduled_task(m[tacheFin], tacheFin);
  //======================== fin patch cas
  // exceptionnel===================================

  //==============================
  // simulation with real values
  //-----------------------------
  // put the real values of amount of work for each task
  realValues(wf);
  // worstValues(wf);

  // double the communication time (in our model, every sent file has to go
  // through a storage center)

  // launch the simulation
  SD_simulate(-1);
  printf("Simulation time valeurs réelles : %f seconds\n", SD_get_clock());

  //==============================
  // data harvesting
  //-----------------
  char fileName[] = "commonResults";
  char typeFich[] = "cyber";
  char *wfName = argv[3];
  char *nomAlgo = argv[1];

  dataGen(fileName, wf, mhc, budget2, cagnotte, deadline, wfName, typeFich,
          hosts, res[1], res[0], ratio, pCSD, nomAlgo, sdTh, seed,
          mCommRootDest, resExpensive[1], resExpensive[0]);

  //==============================================
  // cleaning...
  // tachesSrc.clear();
  //  mapInfosTask.clear();
  freeUtilsMath(); // freeing the generator
  //  xbt_dynar_free_container(&dax);
  deleteWf(wf);

  for (cursor = 0; cursor < total_nhosts; cursor++) {
    free(sg_host_user(hosts[cursor]));
    sg_host_user_set(hosts[cursor], NULL);
  }

  xbt_free(hosts);
  SD_exit();
  return 0;
}
