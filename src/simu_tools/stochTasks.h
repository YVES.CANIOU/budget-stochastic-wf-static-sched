#ifndef _STOCHTAKS
#define _STOCHTAKS

//#include "simdag/simdag.h" // old way to call the lib
#include "simgrid/simdag.h"
#include "utilsMath.h"
#include "xbt.h"

/**
 * Add a stochastic part to a deterministic workflow
 * Basically, from a given mean value of amount of work and a standard
 * deviation, generate a structure with these infos and a random
 * value generated from these data
 **/
typedef struct _InfoStochTask InfoStochTask;
struct _InfoStochTask {
  double realWork;
  double mean;
  double sd;
};

//====================================================
// getters/setters/initialisers/free-ers
//---------------------------------------
/**
 * Initialise a given InfoStochTask
 * with the given values AND generate a value
 * form this data for the realWork part
 **/
void initInfoStochTask(InfoStochTask &ist, SD_task_t task, double sd);

/**
 * Generate the real amount a work needed to totaly execute the task
 *corresponding to the given InfoStochTask
 **/
void initRealWork(InfoStochTask &ist);

double getRealWorkIst(InfoStochTask ist);
double getMeanIst(InfoStochTask ist);
double getSdIst(InfoStochTask ist);

void setRealWorkIst(InfoStochTask &ist, double rw);
void setMeanIst(InfoStochTask &ist, double m);
void setSdIst(InfoStochTask &ist, double sd);

//==============================================
// un brin HS, mais je n'avais pas mieux
//---------------------------------------

/**
 * return true if task is a computational task, false otherwise
 **/
bool isTaskComput(SD_task_t task);

#endif
