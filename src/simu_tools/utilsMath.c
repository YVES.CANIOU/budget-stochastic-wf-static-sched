#include "utilsMath.h"
#include <iostream>

//====================================
// Initialisation and free
//-------------------------

// source of the initialisation function:
// https://www.gnu.org/software/gsl/manual/html_node/Random-number-environment-variables.html
// NB: A fancy explanation about the generation of seeds and a little code from
// Robert Brown are given there :
// http://www.sourceware.org/ml/gsl-discuss/2004-q1/msg00071.html
void iniUtilsMath() {
  const gsl_rng_type *T;
  gsl_rng_env_setup();

  T = gsl_rng_mt19937;
  // NB: exists in these versions too :
  // T = gsl_rng_default;
  // T = gsl_rng_taus;

  r = gsl_rng_alloc(T);
}

void iniUtilsMath(double seed) {
  const gsl_rng_type *T;
  gsl_rng_env_setup();

  // choose a kind of generator
  T = gsl_rng_mt19937;
  // NB: exists in these versions too :
  // T = gsl_rng_default;
  // T = gsl_rng_taus;

  r = gsl_rng_alloc(T);
  gsl_rng_set(r, seed); // seed with seed
}

void freeUtilsMath() { gsl_rng_free(r); }

//=====================================
// Tools
//-----------

double sampleNormal(double mu, double sd) {
  return mu + gsl_ran_gaussian_ziggurat(r, sd);
}

double sampleUniform(double mu, double sd) {
  return mu + gsl_ran_flat(r, -1 * sd, sd);
}

int maxInt(int a, int b) {
  if (a > b) {
    return a;
  } else {
    return b;
  }
}
