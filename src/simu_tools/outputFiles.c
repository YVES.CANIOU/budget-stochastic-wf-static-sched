#include <cstdio>
#include <simgrid/simdag.h>
 
// #include "outputFiles.h"

void credoFile(const char* algoName, const double budgetIni, const double cagnotte) {
  FILE *fich;
  char fileName[] = "credo";

  double totCost = budgetIni - cagnotte;
  double estTime = SD_get_clock();
  fich = fopen(fileName, "a+");
  if (fich != NULL) {
    fprintf(fich, "%s, %f, %f, %f\n", algoName, budgetIni, totCost, estTime);
  }
}