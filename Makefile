.PHONY: all
all:
	cmake -V . -B ./build && cd ./build && make &&	cd ..

.PHONY: fast
fast:
	cmake -V . -B ./build && cd ./build && make -j 2 &&	cd ..


.PHONY: type1
type1:
	cmake -V . -B ./build -DHOST_PRICE=1 && cd ./build && make &&	cd ..

.PHONY: type2
type2:
	cmake -V . -B ./build -DHOST_PRICE=2 && cd ./build && make &&	cd ..



.PHONY: clean
clean:
	rm -rf  ./build/[a-zA-Z0-9]* 
# ./bin/[a-zA-Z0-9]*

.PHONY : resetXP
resetXP:
	cp data/vierge/commonResultsVierge commonResults
	cp data/vierge/credoVierge credo
	cp data/vierge/commonPreciseResultsVierge commonPreciseResults
	rm realTimes
	rm static_sched

.PHONY : resetGen
resetGen:
	cp data/vierge/credoVierge credo
	rm realTimes
	rm static_sched

.PHONY : cleanXP
cleanXP:
	rm commonResults
	rm credo
	rm commonPreciseResults
	rm realTimes
	rm static_sched
